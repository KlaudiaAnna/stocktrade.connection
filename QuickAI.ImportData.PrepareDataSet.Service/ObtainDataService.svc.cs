﻿using QuickAI.ImportData.PrepareDataSet.BLL.GeneralMethods;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.DAL;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.ServiceModel;
using System.Security.AccessControl;
using System.Security.Principal;
namespace QuickAI.ImportData.PrepareDataSet.Service
{
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any, ConcurrencyMode = ConcurrencyMode.Single, InstanceContextMode = InstanceContextMode.Single)]
    public class ObtainDataService : IObtainData, IObtainData2, IDataMongo, IDataFromMongo
    {
        private List<TweetCollectionCompleted> listT;
        public List<TweetCollectionCompleted> listS, rlist;
        public bool GetDataSet()
        {
            // d76ae9d6-efad-4270-90dc-ebf98c1e9d15  -- ID for mutext  
            string IDmutex = "76ae9d6-efad-4270-90dc-ebf98c1e9d15";
            bool handle = false;
            var allowacess = new MutexAccessRule
                (new SecurityIdentifier(WellKnownSidType.WorldSid, null)
                       , MutexRights.FullControl
                       , AccessControlType.Allow);
            var Security = new MutexSecurity();
            Security.AddAccessRule(allowacess);
            bool Newone; // a place to store a return value in Mutext()

            using (Mutex mutex = new Mutex(false, IDmutex, out Newone, Security))
            {
                try
                {
                    try
                    {
                        handle = mutex.WaitOne(5000, false);
                        GenerateDataSets dataSet = new GenerateDataSets();
                        bool success = dataSet.GetDataSet();
                        return success;
                    }
                    catch (Exception ex)
                    {
                        LogtoFile.LogMessagePrint("Exeception in Web Service:" + ex.Message + ex.InnerException);
                        return false;
                    }
                }
                finally
                {
                    if (handle)
                        mutex.ReleaseMutex();
                }
            }
        }
        public List<TweetCollectionCompleted> GetCollectionComplete()
        {
            try
            {
                LogtoFile.LogMessagePrint("downalod twiiter");

                TwitterGenerateData twitterGenerateData = new TwitterGenerateData();
                listT = twitterGenerateData.GetCollectionComplete();
                LogtoFile.LogMessagePrint("check id");

                var idSTock = ReadData();
                LogtoFile.LogMessagePrint("downalod stock");

                StockTwitsGenerateData stockTwitsGenerateData = new StockTwitsGenerateData(idSTock);
                listS = stockTwitsGenerateData.GetCollectionComplete();
                if (listT == null && listS != null)
                {
                    return listS;
                }
                if (listS == null && listT != null)
                {
                    return listT;
                }
                var listy = listS.Concat(listT).ToList();

                return listy;
            }
            catch (Exception ex)
            {
                LogtoFile.LogMessagePrint(ex.Message);
                return null;
            }
        }
        public bool SetData()
        {
            try
            {
                LogtoFile.LogMessagePrint("call methdod sets ");
                rlist = GetCollectionComplete();
                DataToDb db = new DataToDb(rlist);
                LogtoFile.LogMessagePrint("inest to database ");

                var resultData = db.SetData();
                return resultData;
            }
            catch (Exception ex)
            {
                LogtoFile.LogMessagePrint("Exception during setting data to database " + ex.Message);
                return false;
            }
        }
        public long ReadData()
        {
            try
            {
                ReadDataDatabase dataDatabase = new ReadDataDatabase();
                return dataDatabase.ReadData();
            }
            catch (Exception ex)
            {
                LogtoFile.LogMessagePrint("Exception during getting ID from stocktwit" + ex.Message);
                return 0;
            }
        }

        public List<TickersList> TickerlistList()
        {
            try
            {
                GenerateDataSets sets = new GenerateDataSets();
                var tickers = sets.TickerlistList();
                return tickers;
            }
            catch (Exception e)
            {
                LogtoFile.LogMessagePrint(" Exception during downloading Tickerlist " + e.Message + e.InnerException);
                return null;
            }
        }
    }
}

