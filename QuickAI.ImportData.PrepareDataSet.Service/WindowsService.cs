﻿using System.ServiceModel;
using System.Configuration;
using System.Reflection;

namespace QuickAI.ImportData.PrepareDataSet.Service
{
    public class WindowsService : ObtainDataService
    {
        ServiceHost serviceHost = null;
        public WindowsService()
        {
            var servicemame = ConfigurationManager.OpenExeConfiguration(Assembly.GetAssembly(typeof(ObtainDataService)).Location);
        }
        protected void OnStart(string[] args)
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }
            serviceHost = new ServiceHost(typeof(ObtainDataService));
            serviceHost.Open();
        }

        protected void OnStop()
        {
            serviceHost.Close();
        }

    }
}