﻿using System;
using System.Windows.Forms;
namespace QuickAI.ImportData.PrepareDataSet.TestHarness
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            object _locker = new object();

            lock (_locker)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
        }
    }
}
