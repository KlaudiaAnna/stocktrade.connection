﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class ADOSC
    {
        public ADOSC()
        { }

        public class Metadata
        {
            public Metadata()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string Symbol { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string Indicator { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshed { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string Interval { get; set; }
            [JsonProperty(PropertyName = "5.1: FastK Period")]
            public string FastKPeriod { get; set; }
            [JsonProperty(PropertyName = "5.2: SlowK Period")]
            public string SlowKPeriod { get; set; }
            [JsonProperty(PropertyName = "6: Time Zone")]
            public string TimeZone { get; set; }
        }
        public class Technical
        {
            public Technical()
            { }
            [JsonProperty(PropertyName = "ADOSC")]
            public string ADOSC { get; set; }
        }
    }
}