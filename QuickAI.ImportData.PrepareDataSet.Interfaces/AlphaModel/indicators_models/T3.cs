﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class T3
    {
        public T3()
        { }

        public class MetadataT3
        {
            public MetadataT3()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolT3 { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorT3 { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedT3 { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalT3 { get; set; }
            [JsonProperty(PropertyName = "5: Time Period")]
            public string TimePeriodT3 { get; set; }
            [JsonProperty(PropertyName = "6: Volume Factor (vFactor)")]
            public string VolumeFactorT3 { get; set; }
            [JsonProperty(PropertyName = "7: Series Type")]
            public string SeriesTypeT3 { get; set; }
            [JsonProperty(PropertyName = "8: Time Zone")]
            public string TimeZoneT3 { get; set; }
        }
        public class TechnicalT3
        {
            public TechnicalT3()
            { }
            [JsonProperty(PropertyName = "T3")]
            public string T3 { get; set; }
        }
    }
}
