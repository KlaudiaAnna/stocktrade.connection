﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class WMA
    {
        public WMA()
        { }

        public class MetadataWMA
        {
            public MetadataWMA()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolWMA { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorWMA { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedWMA { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalWMA { get; set; }
            [JsonProperty(PropertyName = "5: Time Period")]
            public string TimePeriodWMA { get; set; }
            [JsonProperty(PropertyName = "6: Series Type")]
            public string TSeriesTypeWMA { get; set; }
            [JsonProperty(PropertyName = "7: Time Zone")]
            public string TimeZoneWMA { get; set; }
        }
        public class TechnicalWMA
        {
            public TechnicalWMA()
            { }
            [JsonProperty(PropertyName = "WMA")]
            public string WMA { get; set; }
        }
    }
}