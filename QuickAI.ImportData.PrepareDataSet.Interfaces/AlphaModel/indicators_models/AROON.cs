﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class AROON
    {
        public AROON()
        { }

        public class MetadataAROON
        {
            [JsonProperty(PropertyName = "Symbol")]
            public string SymbolAROON { get; set; }
            [JsonProperty(PropertyName = "Indicator")]
            public string IndicatorAROON { get; set; }
            [JsonProperty(PropertyName = "Last Refreshed")]
            public string LastRefreshedAROON { get; set; }
            [JsonProperty(PropertyName = "Interval")]
            public string IntervalAROON { get; set; }
            [JsonProperty(PropertyName = "Time Period")]
            public string TimePeriodAROON { get; set; }
            [JsonProperty(PropertyName = "Time Zone")]
            public string TimeZoneAROON { get; set; }
        }
        public class TechnicalAROON
        {
            [JsonProperty(PropertyName = "Aroon Down")]
            public string AroonDown { get; set; }
            [JsonProperty(PropertyName = "Aroon Up")]
            public string AroonUp { get; set; }
        }
    }
}