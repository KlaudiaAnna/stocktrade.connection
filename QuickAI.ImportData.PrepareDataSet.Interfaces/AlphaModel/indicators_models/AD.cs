﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class AD
    {
        public AD()
        { }

        public class MetadataAD
        {
            public MetadataAD()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolAd { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorAd { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedAD { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalAd { get; set; }
            [JsonProperty(PropertyName = "5: Time Zone")]
            public string TimePeriodAd { get; set; }
        }

        public class TechnicalAD
        {
            public TechnicalAD()
            { }
            [JsonProperty(PropertyName = "Chaikin A/D")]
            public string AD { get; set; }
        }
    }
}


