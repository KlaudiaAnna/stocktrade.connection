﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class WILLR
    {
        public WILLR()
        { }

        public class MetadataWILLR
        {
            public MetadataWILLR()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string Symbol { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string Indicator { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshed { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string Interval { get; set; }
            [JsonProperty(PropertyName = "5: Time Period")]
            public string TimePeriod { get; set; }
            [JsonProperty(PropertyName = "6: Time Zone")]
            public string TimeZone { get; set; }
        }
        public class TechnicalWILLR
        {
            public TechnicalWILLR()
            { }
            [JsonProperty(PropertyName = "WILLR")]
            public string WILLR { get; set; }
        }
    }
}
