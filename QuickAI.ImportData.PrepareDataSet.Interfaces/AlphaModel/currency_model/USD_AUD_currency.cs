﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class USD_AUD_currency
    {
        public USD_AUD_currency()
            { }
        public class MetadataUSD_AUD
        {
            public MetadataUSD_AUD()
            { }
            [JsonProperty(PropertyName = "1: Information")]
            public string Information { get; set; }
            [JsonProperty(PropertyName = "2: From Symbol")]
            public string FromSymbol { get; set; }
            [JsonProperty(PropertyName = "3: To Symbol")]
            public string ToSymbol { get; set; }
            [JsonProperty(PropertyName = "4: Output Size")]
            public string OutputSize { get; set; }
            [JsonProperty(PropertyName = "5: Last Refreshed")]
            public string LastRefreshed { get; set; }
            [JsonProperty(PropertyName = "6: Time Zone")]
            public string TimeZone { get; set; }
        }
        public class TechnicalUSD_AUD
        {
            [JsonProperty(PropertyName = "1. open")]
            public string OpenUSD_AUD { get; set; }
            [JsonProperty(PropertyName = "2. high")]
            public string HighUSD_AUD { get; set; }
            [JsonProperty(PropertyName = "3. low")]
            public string LowUSD_AUD { get; set; }
            [JsonProperty(PropertyName = "4. close")]
            public string CloseUSD_AUD { get; set; }
        }
    }
}
