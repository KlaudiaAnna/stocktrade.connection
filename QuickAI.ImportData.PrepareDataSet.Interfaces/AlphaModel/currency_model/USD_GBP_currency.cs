﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class USD_GBP_currency
    {
        public USD_GBP_currency()
        { }
        public class MetadataUSD_GBP
        {
            public MetadataUSD_GBP()
            { }
            [JsonProperty(PropertyName = "1: Information")]
            public string Information { get; set; }
            [JsonProperty(PropertyName = "2: From Symbol")]
            public string FromSymbol { get; set; }
            [JsonProperty(PropertyName = "3: To Symbol")]
            public string ToSymbol { get; set; }
            [JsonProperty(PropertyName = "4: Output Size")]
            public string OutputSize { get; set; }
            [JsonProperty(PropertyName = "5: Last Refreshed")]
            public string LastRefreshed { get; set; }
            [JsonProperty(PropertyName = "6: Time Zone")]
            public string TimeZone { get; set; }
        }

        public class TechnicalUSD_GBP
        {
            [JsonProperty(PropertyName = "1. open")]
            public string OpenUSD_GBP { get; set; }
            [JsonProperty(PropertyName = "2. high")]
            public string HighUSD_GBP { get; set; }
            [JsonProperty(PropertyName = "3. low")]
            public string LowUSD_GBP { get; set; }
            [JsonProperty(PropertyName = "4. close")]
            public string CloseUSD_GBP { get; set; }
        }
    }
}
