﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class EUR_USD_currency
    {
        public EUR_USD_currency()
        { }
        public class MetadataEUR_USD
        {
            public MetadataEUR_USD()
            { }
            [JsonProperty(PropertyName = "1: Information")]
            public string InformationEUR_USD { get; set; }
            [JsonProperty(PropertyName = "2: From Symbol")]
            public string FromSymbolEUR_USD { get; set; }
            [JsonProperty(PropertyName = "3: To Symbol")]
            public string ToSymbolEUR_USD { get; set; }
            [JsonProperty(PropertyName = "4: Output Size")]
            public string OutputSizeEUR_USD { get; set; }
            [JsonProperty(PropertyName = "5: Last Refreshed")]
            public string LastRefreshedEUR_USD { get; set; }
            [JsonProperty(PropertyName = "6: Time Zone")]
            public string TimeZoneEUR_USD { get; set; }
        }
        public class TechnicalEUR_USD
        {
            [JsonProperty(PropertyName = "1. open")]
            public string OpenEUR_USD { get; set; }
            [JsonProperty(PropertyName = "2. high")]
            public string HighEUR_USD { get; set; }
            [JsonProperty(PropertyName = "3. low")]
            public string LowEUR_USD { get; set; }
            [JsonProperty(PropertyName = "4. close")]
            public string CloseEUR_USD { get; set; }
        }
    }
}
