﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
namespace QuickAI.ImportData.PrepareDataSet.Interfaces.WorkInProgressReports
{
    [DataContract]
    public class WorkInProgress
    {
        [DataMember(Name = "WorkInProgressElement")]
        public List<WorkInProgressElement> elements { get; set; }
    }
}
