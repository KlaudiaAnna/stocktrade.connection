﻿using System;
using Newtonsoft.Json;
namespace QuickAI.ImportData.PrepareDataSet.Interfaces.JsonLocalModel
{
   public class Ticker_model_json
    {
        [JsonProperty(PropertyName = "TickerName")]
        public string Ticker { get; set; }
        [JsonProperty(PropertyName = "Date")]
        public DateTime Date { get; set; }



    }
}
