﻿using Newtonsoft.Json;
namespace TwitterTest.JsonData
{
    public class Entities
    {
        public Entities()
        { }
        [JsonProperty(PropertyName = "symbols")]
        public Symbols[] Symbols { get; set; }
    }
}