﻿using Newtonsoft.Json;
using System.Collections.Generic;
namespace TwitterTest.JsonData
{
    public class Tweetdata
    {
        public Tweetdata()
        { }
        [JsonProperty(PropertyName = "statuses")]
        public List<Tweet> Statuses { get; set; }
    }
}

