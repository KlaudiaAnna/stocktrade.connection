﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel
{ 
    public class Symbols
    {
        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }
    }
}
