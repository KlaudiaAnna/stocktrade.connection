﻿using System;
using Newtonsoft.Json;
namespace QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel
{
    public class Retweed
    {
        [JsonProperty(PropertyName = "id_str")]
        public Int64 Idstr_R { get; set; }        
        [JsonProperty(PropertyName = "full_text")]
        public string Fulltext_r { get; set; }
        [JsonProperty(PropertyName = "entities")]
        public Entities Entities_R { get; set; }
        [JsonProperty(PropertyName = "user")]
        public UserObject UserObject_R { get; set; }

    }
}