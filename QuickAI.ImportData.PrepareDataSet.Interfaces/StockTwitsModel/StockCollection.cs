﻿using System.Collections.Generic;
using Newtonsoft.Json;
namespace QuickAI.ImportData.PrepareDataSet.Interfaces.StockTwitsModel
{
    public class StockCollection
    {
        public StockCollection()
        { }
        [JsonProperty(PropertyName = "messages")]
        public List<StockData> Stockdata { get; set; }
        [JsonProperty(PropertyName = "symbol")]
        public Symbol Symbol { get; set; }

    }
}
