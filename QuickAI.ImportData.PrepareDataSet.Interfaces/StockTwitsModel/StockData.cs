﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace QuickAI.ImportData.PrepareDataSet.Interfaces.StockTwitsModel
{
    public class StockData
    {
        [JsonProperty(PropertyName = "id")]
        public Int64 Idbody { get; set; }
        [JsonProperty(PropertyName = "body")]
        public string Body { get; set; }
        [JsonProperty(PropertyName = "created_at")]
        public DateTime Created_at { get; set; }
        [JsonProperty(PropertyName = "user")]
        public User User { get; set; }
    }
}
