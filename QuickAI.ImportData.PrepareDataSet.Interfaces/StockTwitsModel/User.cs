﻿using System;
using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.StockTwitsModel
{
    public class User
    {
        [JsonProperty(PropertyName = "id")]
        public Int64 IdUser { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string NameUser { get; set; }
    }
}
