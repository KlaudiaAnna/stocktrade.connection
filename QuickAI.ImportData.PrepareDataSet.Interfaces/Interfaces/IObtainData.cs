﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces
{
    [ServiceContract]
    public interface IObtainData
    {
        [OperationContract(Name = "ReturnedTrueorFalse")]
        bool GetDataSet();

        [OperationContract(Name = "TickerswithOpenCloseBol")]
        List<TickersList> TickerlistList();
    }
    [ServiceContract]
    public interface IObtainData2
    {
        [OperationContract(Name = "Tweets")]
        List<TweetCollectionCompleted> GetCollectionComplete();
    }

    [ServiceContract]
    public interface IDataMongo
    {
        [OperationContract(Name = "SetsDatabse")]
        bool SetData();
    }

    [ServiceContract(Name = "IdStocktwits")]
    public interface IDataFromMongo
    {
        [OperationContract]
        long ReadData();
    }

    [DataContract]
    public class TweetCollectionCompleted
    {
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public Int64 IdTweet { get; set; }
        [DataMember]
        public string Text { get; set; }
        [DataMember]
        public string Symbol { get; set; }
        [DataMember]
        public Int64 IdUser { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Portal { get; set; }
        [DataMember]
        public string DateInsertedDb { get; set; }
        [DataMember]
        public string Human_Rating { get; set; }
        [DataMember]
        public string Neural_Rating { get; set; }
    }

    [DataContract]
    public class TickersList
    {
        [DataMember]
        public string TickerName { get; set; }
    }

}