﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class PPO
    {
        public PPO()
        { }

        public class Metadata
        {
            public Metadata()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string Symbol { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string Indicator { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshed { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string Interval { get; set; }
            [JsonProperty(PropertyName = "5.1: Fast Period")]
            public string FastPeriod { get; set; }
            [JsonProperty(PropertyName = "5.2: Slow Period")]
            public string SlowPeriod { get; set; }
            [JsonProperty(PropertyName = "5.3: MA Type")]
            public string Matype { get; set; }
            [JsonProperty(PropertyName = "6: Series Type")]
            public string TimePeriod { get; set; }
            [JsonProperty(PropertyName = "7: Time Zone")]
            public string TimeZone { get; set; }
        }
        public class Technical
        {
            public Technical()
            { }
            [JsonProperty(PropertyName = "PPO")]
            public string PPO { get; set; }
        }
    }
}