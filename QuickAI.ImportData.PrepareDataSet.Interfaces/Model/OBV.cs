﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class Obv
    {
        public Obv()
        { }

        public class MetadataObv
        {
            public MetadataObv()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolObv { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorObv { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedObv { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalObv { get; set; }
            [JsonProperty(PropertyName = "5: Time Zone")]
            public string TimePeriodObv { get; set; }
        }

        public class TechnicalOBV
        {
            public TechnicalOBV()
            { }
            [JsonProperty(PropertyName = "OBV")]
            public string OBV { get; set; }

        }
    }
}