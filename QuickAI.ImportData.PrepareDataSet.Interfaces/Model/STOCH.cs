﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class STOCH
    {

        public STOCH()
        { }


        public class MetadataSTOCH
        {
            public MetadataSTOCH()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolSTOCH { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorSTOCH { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedSTOCH { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalSTOCH { get; set; }
            [JsonProperty(PropertyName = "5.1: FastK Period")]
            public string FastKPeriodSTOCH { get; set; }
            [JsonProperty(PropertyName = "5.2: SlowK Period")]
            public string SlowKPeriodSTOCH { get; set; }
            [JsonProperty(PropertyName = "5.3: SlowK MA Type")]
            public string SlowKMATypeSTOCH { get; set; }
            [JsonProperty(PropertyName = "5.4: SlowD Period")]
            public string SlowDPeriod { get; set; }
            [JsonProperty(PropertyName = "5.5: SlowD MA Type")]
            public string SlowDMAType { get; set; }
            [JsonProperty(PropertyName = "6: Time Zone")]
            public string TimePeriodSTOCH { get; set; }
        }
        public class TechnicalSTOCH
        {
            public TechnicalSTOCH()
            { }
            [JsonProperty(PropertyName = "SlowK")]
            public string SlowK { get; set; }
            [JsonProperty(PropertyName = "SlowD")]
            public string SlowD { get; set; }

        }
    }
}
