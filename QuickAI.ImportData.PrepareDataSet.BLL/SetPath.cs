﻿using System;
using System.Configuration;
using System.IO;
namespace QuickAI.ImportData.PrepareDataSet.BLL
{
    public class SetPath
    {
        protected string path = ConfigurationManager.AppSettings["Path"];
        protected string pathLog = ConfigurationManager.AppSettings["PathtoLog"];
        protected string NewPath;
        protected SetPath(string setpath)
        {
            if (string.IsNullOrEmpty(setpath))
            {

            }
            NewPath = ConfigurationManager.AppSettings[setpath];
        }
        public string SetPathToFile(string fileName)
        {
            string someFile = Path.Combine(Environment.CurrentDirectory, NewPath, fileName);
            return someFile;
        }
    }
}

