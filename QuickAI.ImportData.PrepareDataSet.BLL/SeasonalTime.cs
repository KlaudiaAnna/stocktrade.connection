﻿using System;
namespace QuickAI.ImportData.PrepareDataSet.BLL
{
    public class SeasonalTime
    {
        public Context context;
        public SeasonalTime(Context c)
        {
            context = c;
        }
        public void Add_Monts_Day()
        {
            for (int i = 0; i < context.Elements.Count; i++)
            {
                var elem_i = context.Elements[i];
                string data = elem_i.Data; // from context element
                DateTime d = DateTime.Parse(data);
                int day = d.Day;
                int month = d.Month;
                if (day != 0 && month != 0)
                {
                    elem_i.Values = string.Concat(elem_i.Values, ",", day, ",", month);
                }
            }
        }
    }
}
