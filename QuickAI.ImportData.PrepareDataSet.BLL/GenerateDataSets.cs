﻿using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation;
using QuickAI.ImportData.PrepareDataSet.BLL.Download;
using QuickAI.ImportData.PrepareDataSet.BLL.NormalizeValues;
using QuickAI.ImportData.PrepareDataSet.BLL.Worker;
using QuickAI.ImportData.PrepareDataSet.BLL.Worker.currency;
using QuickAI.ImportData.PrepareDataSet.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.CandleFormation;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using System;
using System.Collections.Generic;

namespace QuickAI.ImportData.PrepareDataSet.BLL
{
    public class GenerateDataSets : IObtainData
    {
        public GenerateDataSets()
        { }

        public bool GetDataSet()
        {
            bool success;
            return success = false;
            try
            {
                ApplicationConfiguration cfg = Configuration.GetApplicationConfig();
             //   foreach (TickerInfo ticker in cfg.Tickers)
                    //{
                    //    Context context = new Context();
                    //    QuickIAWebRequest webR = new QuickIAWebRequest(ticker.Name, "TIME_SERIES_DAILY");

                    //    string data = webR.Download();

                    //    TickerData tickerData = JsonConvert.DeserializeObject<TickerData>(data);
                    //    tickerData.CleanseData();

                    //    if (tickerData.TimeSeriesData == null)
                    //    {
                    //        throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ticker.Name));
                    //    }
                    //    tickerData.AddToContext(context, false);

                    //    /***********************************TICKER INDEX************************************************************************************/

                    //    webR = new QuickIAWebRequest(ticker.Index, "Time_Series_DAILY");

                    //    string idate = webR.Download();

                    //    tickerData = JsonConvert.DeserializeObject<TickerData>(idate);
                    //    tickerData.CleanseData();

                    //    if (tickerData.TimeSeriesData == null)
                    //    {
                    //        throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ticker.Index));
                    //    }
                    //    tickerData.AddToContext(context, true);


                    //    /***********************************************************Indicators*************************************************************/
                    //    foreach (IndicatorInfo ii in cfg.Indicators)
                    //    {
                    //        webR = new QuickIAWebRequest(ii.Name, "TIME_SERIES_DAILY");

                    //        if (ii.Name == "SMA")
                    //        {
                    //            webR.Url = IndicatorSMA.GetUrl(ticker.Name);
                    //            string smaData = webR.Download();

                    //            IndicatorSMA smaDataIndicator = JsonConvert.DeserializeObject<IndicatorSMA>(smaData);
                    //            smaDataIndicator.CleanseData();

                    //            if (smaDataIndicator.TimeSeriesData == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ii.Name));
                    //            }
                    //            smaDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "AD")
                    //        {
                    //            webR.Url = IndicatorAD.GetUrl(ticker.Name);
                    //            string adData = webR.Download();

                    //            IndicatorAD ADDataIndicator = JsonConvert.DeserializeObject<IndicatorAD>(adData);
                    //            ADDataIndicator.CleanseData();

                    //            if (ADDataIndicator.TechnicalADdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ii.Name));
                    //            }
                    //            ADDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "ADX")
                    //        {
                    //            webR.Url = IndicatorADX.GetUrl(ticker.Name);
                    //            string ADXata = webR.Download();

                    //            IndicatorADX ADXdataIndicator = JsonConvert.DeserializeObject<IndicatorADX>(ADXata);
                    //            ADXdataIndicator.CleanseData();

                    //            if (ADXdataIndicator.TechnicalADXdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ii.Name));
                    //            }
                    //            ADXdataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "BBANDS")
                    //        {
                    //            webR.Url = IndicatorBBANDS.GetUrl(ticker.Name);
                    //            string BBANDSData = webR.Download();

                    //            IndicatorBBANDS BBANDSdataIndicator = JsonConvert.DeserializeObject<IndicatorBBANDS>(BBANDSData);
                    //            BBANDSdataIndicator.CleanseData();

                    //            if (BBANDSdataIndicator.TechnicalBBANDSdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ii.Name));
                    //            }
                    //            BBANDSdataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "T3")
                    //        {
                    //            webR.Url = IndicatorT3.GetUrl(ticker.Name);
                    //            string T3Data = webR.Download();

                    //            IndicatorT3 T3dataIndicator = JsonConvert.DeserializeObject<IndicatorT3>(T3Data);
                    //            T3dataIndicator.CleanseData();

                    //            if (T3dataIndicator.TechnicalT3data == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ii.Name));
                    //            }
                    //            T3dataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "MACD")
                    //        {
                    //            webR.Url = IndicatorMACD.GetUrl(ticker.Name);
                    //            string MACDData = webR.Download();

                    //            IndicatorMACD MACDdataIndicator = JsonConvert.DeserializeObject<IndicatorMACD>(MACDData);
                    //            MACDdataIndicator.CleanseData();

                    //            if (MACDdataIndicator.TechnicalMACDdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ii.Name));
                    //            }
                    //            MACDdataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "OBV")
                    //        {
                    //            webR.Url = IndicatotOBV.GetUrl(ticker.Name);
                    //            string OBVData = webR.Download();

                    //            IndicatotOBV OBVdataIndicator = JsonConvert.DeserializeObject<IndicatotOBV>(OBVData);
                    //            OBVdataIndicator.CleanseData();

                    //            if (OBVdataIndicator.TechnicalOBVdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ii.Name));
                    //            }
                    //            OBVdataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "STOCH")
                    //        {
                    //            webR.Url = IndicatorStoch.GetUrl(ticker.Name);
                    //            string StochData = webR.Download();

                    //            IndicatorStoch StochdataIndicator = JsonConvert.DeserializeObject<IndicatorStoch>(StochData);
                    //            StochdataIndicator.CleanseData();

                    //            if (StochdataIndicator.TechnicalSTOCHdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ii.Name));
                    //            }
                    //            StochdataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "RSI")
                    //        {
                    //            webR.Url = IndicatorRSI.GetUrl(ticker.Name);
                    //            string RSIData = webR.Download();

                    //            IndicatorRSI RSIdataIndicator = JsonConvert.DeserializeObject<IndicatorRSI>(RSIData);
                    //            RSIdataIndicator.CleanseData();

                    //            if (RSIdataIndicator.TechnicalRSIdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ii.Name));
                    //            }

                    //            RSIdataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "EMA")
                    //        {
                    //            webR.Url = IndicatorEMA.GetUrl(ticker.Name);
                    //            string EMAData = webR.Download();

                    //            IndicatorEMA EMAdataIndicator = JsonConvert.DeserializeObject<IndicatorEMA>(EMAData);
                    //            EMAdataIndicator.CleanseData();

                    //            if (EMAdataIndicator.TechnicalEMAdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ii.Name));
                    //            }
                    //            EMAdataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "CCI")
                    //        {
                    //            webR.Url = IndicatorCCI.GetUrl(ticker.Name);
                    //            string CCIData = webR.Download();

                    //            IndicatorCCI CCIdataIndicator = JsonConvert.DeserializeObject<IndicatorCCI>(CCIData);
                    //            CCIdataIndicator.CleanseData();

                    //            if (CCIdataIndicator.TechnicalCCIdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}!", ii.Name));
                    //            }
                    //            CCIdataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "WMA")
                    //        {
                    //            webR.Url = IndicatorWMA.GetUrl(ticker.Name);
                    //            string WMAData = webR.Download();

                    //            IndicatorWMA WMAdataIndicator = JsonConvert.DeserializeObject<IndicatorWMA>(WMAData);
                    //            WMAdataIndicator.CleanseData();

                    //            if (WMAdataIndicator.TechnicalWMAdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            WMAdataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "AROON")
                    //        {
                    //            webR.Url = IndicatorAROON.GetUrl(ticker.Name);
                    //            string AroonData = webR.Download();

                    //            IndicatorAROON AroonDataIndicator = JsonConvert.DeserializeObject<IndicatorAROON>(AroonData);
                    //            AroonDataIndicator.CleanseData();

                    //            if (AroonDataIndicator.TechnicalAROONdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            AroonDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "MFI")
                    //        {
                    //            webR.Url = IndicatorMFI.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorMFI MFIDataIndicator = JsonConvert.DeserializeObject<IndicatorMFI>(Data);
                    //            MFIDataIndicator.CleanseData();

                    //            if (MFIDataIndicator.TechnicalMFIdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            MFIDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "ULTOSC")
                    //        {
                    //            webR.Url = IndicatorULTOSC.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorULTOSC ULTOSCDataIndicator = JsonConvert.DeserializeObject<IndicatorULTOSC>(Data);
                    //            ULTOSCDataIndicator.CleanseData();

                    //            if (ULTOSCDataIndicator.TechnicalULTOSCdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            ULTOSCDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "TRIX")
                    //        {
                    //            webR.Url = IndicatorTRIX.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorTRIX TRIXDataIndicator = JsonConvert.DeserializeObject<IndicatorTRIX>(Data);
                    //            TRIXDataIndicator.CleanseData();

                    //            if (TRIXDataIndicator.TechnicalTRIXdata == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            TRIXDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "DX")
                    //        {
                    //            webR.Url = IndicatorDX.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorDX DXDataIndicator = JsonConvert.DeserializeObject<IndicatorDX>(Data);
                    //            DXDataIndicator.CleanseData();

                    //            if (DXDataIndicator.TechnicalDX == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            DXDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "WILLR")
                    //        {
                    //            webR.Url = IndicatorWILLR.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorWILLR WILLRDataIndicator = JsonConvert.DeserializeObject<IndicatorWILLR>(Data);
                    //            WILLRDataIndicator.CleanseData();

                    //            if (WILLRDataIndicator.TechnicalWILLR == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            WILLRDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "ADXR")
                    //        {
                    //            webR.Url = IndicatorADXR.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorADXR ADXRDataIndicator = JsonConvert.DeserializeObject<IndicatorADXR>(Data);
                    //            ADXRDataIndicator.CleanseData();

                    //            if (ADXRDataIndicator.TechnicalADXR == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            ADXRDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "PPO")
                    //        {
                    //            webR.Url = IndicatorPPO.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorPPO PPODataIndicator = JsonConvert.DeserializeObject<IndicatorPPO>(Data);
                    //            PPODataIndicator.CleanseData();

                    //            if (PPODataIndicator.TechnicalPPO == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            PPODataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "MOM")
                    //        {
                    //            webR.Url = IndicatorMOM.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorMOM MOMDataIndicator = JsonConvert.DeserializeObject<IndicatorMOM>(Data);
                    //            MOMDataIndicator.CleanseData();

                    //            if (MOMDataIndicator.TechnicalMOM == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            MOMDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "BOP")
                    //        {
                    //            webR.Url = IndicatorBOP.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorBOP BOPDataIndicator = JsonConvert.DeserializeObject<IndicatorBOP>(Data);
                    //            BOPDataIndicator.CleanseData();

                    //            if (BOPDataIndicator.TechnicalBOP == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            BOPDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "CMO")
                    //        {
                    //            webR.Url = IndicatorCMO.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorCMO CMODataIndicator = JsonConvert.DeserializeObject<IndicatorCMO>(Data);
                    //            CMODataIndicator.CleanseData();

                    //            if (CMODataIndicator.TechnicalCMO == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            CMODataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "ROC")
                    //        {
                    //            webR.Url = IndicatorROC.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorROC ROCDataIndicator = JsonConvert.DeserializeObject<IndicatorROC>(Data);
                    //            ROCDataIndicator.CleanseData();

                    //            if (ROCDataIndicator.TechnicalROC == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            ROCDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "PLUS_DI")
                    //        {
                    //            webR.Url = IndicatorPlusDi.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorPlusDi PlusDiDataIndicator = JsonConvert.DeserializeObject<IndicatorPlusDi>(Data);
                    //            PlusDiDataIndicator.CleanseData();

                    //            if (PlusDiDataIndicator.TechnicalPlusDi == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            PlusDiDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "MINUS_DM")
                    //        {
                    //            webR.Url = IndicatorMINUS_DM.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorMINUS_DM MINUSDMDataIndicator = JsonConvert.DeserializeObject<IndicatorMINUS_DM>(Data);
                    //            MINUSDMDataIndicator.CleanseData();

                    //            if (MINUSDMDataIndicator.TechnicalMINUS_DM == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            MINUSDMDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "ADOSC")
                    //        {
                    //            webR.Url = IndicatorADOSC.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorADOSC ADOSCDataIndicator = JsonConvert.DeserializeObject<IndicatorADOSC>(Data);
                    //            ADOSCDataIndicator.CleanseData();

                    //            if (ADOSCDataIndicator.TechnicalADOSC == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            ADOSCDataIndicator.AddToContext(context);
                    //        }
                    //        if (ii.Name == "HT_TRENDLINE")
                    //        {
                    //            webR.Url = IndicatorHT_TRENDLINE.GetUrl(ticker.Name);
                    //            string Data = webR.Download();

                    //            IndicatorHT_TRENDLINE HTDataIndicator = JsonConvert.DeserializeObject<IndicatorHT_TRENDLINE>(Data);
                    //            HTDataIndicator.CleanseData();

                    //            if (HTDataIndicator.TechnicalHT == null)
                    //            {
                    //                throw new NullReferenceException(string.Format("Could not get the ticker date for {0}", ii.Name));
                    //            }
                    //            HTDataIndicator.AddToContext(context);
                    //        }
                    //    }

                    //    /*******************************************volume_divided ********************************************************************************/

                    //    NormalizeADOSC adosc = new NormalizeADOSC(context);
                    //    adosc.ADOSCaluculate();

                    //    NormalizeVolume v = new NormalizeVolume(context); // normalize volume
                    //    v.VolumeCaluculate();

                    //    NormalizeVolumeIndex vindex = new NormalizeVolumeIndex(context); // NORMALIZE INDEX VOLUME
                    //    vindex.VolumeIndexCaluculate();

                    //    NormalizeAD ad = new NormalizeAD(context);
                    //    ad.ADCaluculate();

                    //    NormalizeOBV obv = new NormalizeOBV(context);
                    //    obv.OBVcalculate();

                    //    /**********************************************Add day and month************************************************************/

                    //    SeasonalTime seasonal = new SeasonalTime(context);
                    //    seasonal.Add_Monts_Day();

                    //    /**********************************************call formations*****************************************************************************/

                    //    Hammer h = new Hammer(context);
                    //    h.ProcessHammer();

                    //    HangedMan hanged = new HangedMan(context);
                    //    hanged.ProcessHangedMan();

                    //    Hossa_h hossa_H = new Hossa_h(context);
                    //    hossa_H.Process_Hossa_h();

                    //    Bessa_b bessa_b = new Bessa_b(context);
                    //    bessa_b.Process_Bessa_b();

                    //    InsideBar ib = new InsideBar(context);
                    //    ib.Process_InsideBar();

                    //    DojiStar ds = new DojiStar(context);
                    //    ds.Process_Doji();

                    //    MorningStar ms = new MorningStar(context);
                    //    ms.ProcessMorningStar();

                    //    EveningStar ev = new EveningStar(context);
                    //    ev.ProcessEveningStar();

                    //    /**************************************Currency**************************************************************************************/
                    //    // EUR_USD
                    //    webR.Url = EUR_USD_currency_method.GetUrl();
                    //    string EUR_USD = webR.Download();

                    //    EUR_USD_currency_method EUR_USD_CUR = JsonConvert.DeserializeObject<EUR_USD_currency_method>(EUR_USD);
                    //    EUR_USD_CUR.CleanseData();
                    //    if (EUR_USD_CUR.TechnicalEUR_USD == null)
                    //    {
                    //        throw new NullReferenceException(string.Format("Could not get the currency"));
                    //    }
                    //    EUR_USD_CUR.AddToContext(context);

                    //    //USD_AUD
                    //    webR.Url = USD_AUD_currency_method.GetUrl();
                    //    string USD_AUD = webR.Download();

                    //    USD_AUD_currency_method USD_AUD_CUR = JsonConvert.DeserializeObject<USD_AUD_currency_method>(USD_AUD);
                    //    USD_AUD_CUR.CleanseData();
                    //    if (USD_AUD_CUR.TechnicalUSD_AUD == null)
                    //    {
                    //        throw new NullReferenceException(string.Format("Could not get the currency"));
                    //    }
                    //    USD_AUD_CUR.AddToContext(context);

                    //    //USD_GBP
                    //    webR.Url = USD_GBP_currency_method.GetUrl();
                    //    string USD_GBP = webR.Download();

                    //    USD_GBP_currency_method USD_GBP_CUR = JsonConvert.DeserializeObject<USD_GBP_currency_method>(USD_GBP);
                    //    USD_GBP_CUR.CleanseData();
                    //    if (USD_GBP_CUR.TechnicalUSD_GBP == null)
                    //    {
                    //        throw new NullReferenceException(string.Format("Could not get the currency"));
                    //    }
                    //    USD_GBP_CUR.AddToContext(context);

                    //    //USD_JPY
                    //    webR.Url = USD_JPY_currency_method.GetUrl();
                    //    string USD_JPY = webR.Download();

                    //    USD_JPY_currency_method USD_JPY_CUR = JsonConvert.DeserializeObject<USD_JPY_currency_method>(USD_JPY);
                    //    USD_JPY_CUR.CleanseData();
                    //    if (USD_JPY_CUR.TechnicalUSD_JPY == null)
                    //    {
                    //        throw new NullReferenceException(string.Format("Could not get the currency"));
                    //    }
                    //    USD_JPY_CUR.AddToContext(context);

                    //    /**********************************************save to files***************************************************************************************************************/

                    //    //context.SaveToFile(ticker.Name);
                    //    context.Save_Reversed(ticker.Name);
                    //    //context.Save_Classifier(ticker.Name);
                    //}
 

                return success = true;
            }
            catch (Exception ex)
            {
                var e =  ex.Message;
                success = false;
                return success;
            }
        }

        public string InsertData(List<object> list)
        {
            throw new NotImplementedException();
        }

    }
}



