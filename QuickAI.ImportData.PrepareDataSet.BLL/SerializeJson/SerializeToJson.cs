﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using QuickAI.ImportData.PrepareDataSet.Interfaces.WorkInProgressReports;
namespace QuickAI.ImportData.PrepareDataSet.BLL.SerializeJson
{
    public class SerializeToJson
    {
        public string path = ConfigurationManager.AppSettings["Json_Path"];
        private string ticker;
        public SerializeToJson(string ticker)
        {
            this.ticker = ticker;
        }
        JsonSerializer serializer = new JsonSerializer();
        List<WorkInProgress> elem = new List<WorkInProgress>();
        List<WorkInProgressElement> el = new List<WorkInProgressElement>();
        public void SetoJson(bool orig, bool rever, bool clas)
        {
            string jpath = path + "Alpha_" + DateTime.Now.ToString("yyyy-dd-MM") + ".json";
            if (File.Exists(jpath))
            {
                var jsondata = File.ReadAllText(jpath);
                var read = JsonConvert.DeserializeObject<List<WorkInProgress>>(jsondata);

                read.Add(new WorkInProgress() { elements = el });
                el.Add(new WorkInProgressElement() { TickerName = ticker, Reversed = rever, Clasifier = clas, Original = orig, Date = DateTime.Now.ToShortDateString() });
                using (StreamWriter file = new StreamWriter(jpath))
                using (JsonWriter writer = new JsonTextWriter(file))
                {
                    serializer.Serialize(writer, read);
                }
            }
            else
            {
                elem.Add(new WorkInProgress() { elements = el });
                el.Add(new WorkInProgressElement() { TickerName = ticker, Reversed = rever, Clasifier = clas, Original = orig, Date = DateTime.Now.ToShortDateString() });
                using (StreamWriter file = new StreamWriter(jpath))
                using (JsonWriter writer = new JsonTextWriter(file))
                {
                    serializer.Serialize(writer, elem);
                }
            }
        }
    }
}

