﻿using System;
using System.Collections.Generic;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Ticker_Failed;
using Newtonsoft.Json;
using System.IO;
using System.Configuration;
namespace QuickAI.ImportData.PrepareDataSet.BLL.SerializeJson
{
    public class Ticker_Failed_json
    {
        public string path = ConfigurationManager.AppSettings["Json_Path"];
        private string ticker;
        public Ticker_Failed_json(string ticker)
        {
            this.ticker = ticker;
        }
        JsonSerializer serializer = new JsonSerializer();
        List<Tickers_failed_elements> elem = new List<Tickers_failed_elements>();
        List<Ticker_failed_l> el = new List<Ticker_failed_l>();
        public void SetoJson()
        {
            string jpath = path + "TickerFailed_" + DateTime.Now.ToString("yyyy-dd-MM") + ".json";
            if (File.Exists(jpath))
            {
                var jsondata = File.ReadAllText(jpath);
                var read = JsonConvert.DeserializeObject<List<Tickers_failed_elements>>(jsondata);

                read.Add(new Tickers_failed_elements() { Failed_tickers = el});
                el.Add(new Ticker_failed_l() { Tickername = ticker, Index = "SPY"});
                using (StreamWriter file = new StreamWriter(jpath))
                using (JsonWriter writer = new JsonTextWriter(file))
                {
                    serializer.Serialize(writer, read);
                }
            }
            else
            {
                elem.Add(new Tickers_failed_elements() { Failed_tickers = el });
                el.Add(new Ticker_failed_l() { Tickername = ticker, Index = "SPY" });
                using (StreamWriter file = new StreamWriter(jpath))
                using (JsonWriter writer = new JsonTextWriter(file))
                {
                    serializer.Serialize(writer, elem);
                }
            }
        }
    }

}

