﻿using System;
using QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel;
using System.Collections.Generic;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;

namespace QuickAI.ImportData.PrepareDataSet.BLL.MethodTwitter
{
    public static class CheckStatusofTweet
    {
        public static List<Tweet> CheckStatus(List<Tweet> list)
        {
            try
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    if (list[i].Retweeted != null)
                    {
                        list[i].Text = list[i].Retweeted.Fulltext_r;
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                LogtoFile.LogMessagePrint(string.Format("Problem with method Check Status of Tweet{0},{1}", ex.InnerException, ex.Message));
                return null;
            }
        }
    }
}




