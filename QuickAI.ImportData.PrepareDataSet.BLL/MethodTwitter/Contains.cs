﻿using QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel;
namespace QuickAI.ImportData.PrepareDataSet.BLL.MethodTwitter
{
    public class Contains
    {
        public string ticker = string.Empty;
        public Contains(string ticker)
        {
            this.ticker = ticker;
        }
        public void TickerOnlyOne(Tweetdata tweetdata)
        {
            for (int i = tweetdata.Statuses.Count - 1; i >= 0; i--)
            {
                var symbol = tweetdata.Statuses[i].Entities.Symbols;
                for (var j = symbol.Count - 1 ; j >= 0;)
                {
                    if (tweetdata.Statuses[i].Entities.Symbols[j].Text == ticker && symbol.Count == 1)
                    {
                        break;
                    }
                    tweetdata.Statuses.RemoveAt(i);
                    break;
                }
            }
        }
    }
}




