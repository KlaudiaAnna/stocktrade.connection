﻿using QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel;
using System;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
namespace QuickAI.ImportData.PrepareDataSet.BLL.MethodTwitter
{
    public static class CountWordinTweet
    {
        public static void Words(Tweetdata tweetdata)
        {
            for (int i = tweetdata.Statuses.Count - 1; i >= 0; i--)
            {
                string text = tweetdata.Statuses[i].Text;
                string[] separator = text.Split(' ');
                int wordsCount = text.Split(separator, StringSplitOptions.RemoveEmptyEntries).Length;
                if (wordsCount < Configuration.GetApplicationConfig().CounterWords)
                {
                    tweetdata.Statuses.RemoveAt(i);
                }
            }
        }
    }
}