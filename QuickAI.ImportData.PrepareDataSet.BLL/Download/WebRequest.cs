﻿using System;
using System.Text;
using System.Net;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using System.Threading;
namespace QuickAI.ImportData.PrepareDataSet.BLL.Download
{
    public sealed class QuickIaWebRequest
    {
        private readonly string ticker = string.Empty;
        private readonly string function = string.Empty;
        public string url;

        public QuickIaWebRequest(string ticker, string function)
        {
            this.ticker = ticker; 
            this.function = function;
        }
        public string Download()
        {
            try
            {
                LogtoFile.LogMessagePrint(string.Format("Start download for ticker {0} and function {1}", ticker,
                    function));
                bool doWork = true;
                string output = string.Empty;
                int counter = 0;
                while (doWork)
                {
                    /*fiddler only to check (30 days trial versions)*/

                    if (counter > 6)
                     return output;
                    string uri = Url + "&apikey=" + Configuration.GetApplicationConfig().ApiKeyPremium;
                    LogtoFile.LogMessagePrint("Url request: " + uri);
                    LogtoFile.LogMessagePrint("Download data");
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                    request.ContentType = "application/json";
                    request.UserAgent = "user-agent";
                    request.Headers.Add("cache-control", "no-cache");
                   // request.Headers.Add("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                    request.Method = "Get";
                    output = string.Empty;
                    using (var response = request.GetResponse())
                    {
                        LogtoFile.LogMessagePrint("Get response");
                        using (var stream = new System.IO.StreamReader(response.GetResponseStream(),
                            Encoding.GetEncoding(1252)))
                        {
                            output = stream.ReadToEnd();
                        }
                    }
                    if (output.Contains("Thank you for using Alpha Vantage!"))
                    {
                        LogtoFile.LogMessagePrint(
                            string.Format("output contains: Thank you for using Alpha Vantage!"));
                        LogtoFile.LogMessagePrint("start thread and next go to sleep");
              
                        Thread.Sleep(15000);
                        doWork = true;
                        counter++;
                        LogtoFile.LogMessagePrint("Iteration:" + counter + "stop sleep");

                    }
                    else
                        doWork = false;
                }
                LogtoFile.LogMessagePrint("Returned value of output");
                return output;
            }
            catch (Exception ex)
            {
                LogtoFile.LogMessagePrint(string.Format("Exception occured while running WebRequest: {0}, {1}",
                    ex.Message, ex.InnerException));
                return null;
            }
        }

        public string Url
        {
            get
            {
                if (string.IsNullOrEmpty(url))
                    url = Configuration.GetApplicationConfig().ApiUrl + function + "&symbol=" + ticker + "&outputsize=full";
                return url;
            }
            set
            {
                url = value;
            }
        }
    }
}










