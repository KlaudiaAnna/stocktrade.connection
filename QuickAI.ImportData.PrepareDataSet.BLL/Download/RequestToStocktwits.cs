﻿using System;
using System.Net;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.Interfaces.StockTwitsModel;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
namespace QuickAI.ImportData.PrepareDataSet.BLL.Download
{
    public class RequestToStocktwits
    {
        public string ticker;
        public long idSTock;
        public string content;
        public RequestToStocktwits(string ticker, long idSTock)
        {
            this.idSTock = idSTock;
            this.ticker = ticker;
        }
        public StockCollection Request()
        {
            try
            {
                var stocknewApiUrlStocktwits = Configuration.GetApplicationConfig().ApiUrlStocktwits;
                WebClient client = new WebClient();
                string url = stocknewApiUrlStocktwits + ticker + ".json";
                if (idSTock == 0)
                {
                    content = client.DownloadString(url);
                }
                else
                {
                    string urlParametr = url + "?since=" + idSTock;
                    content = client.DownloadString(urlParametr);
                }
                WebUtility.HtmlDecode(content);
                var tojson = JsonConvert.DeserializeObject<StockCollection>(content);
                return tojson;
            }
            catch (Exception e)
            {
                LogtoFile.LogMessagePrint(e.Message);
                return null;
            }
        }
    }
}
