﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
namespace QuickAI.ImportData.PrepareDataSet.BLL.Download
{
    public class RequestToTwitter
    {
        private readonly string ticker = string.Empty;
        public RequestToTwitter(string ticker)
        {
            this.ticker = ticker;
        }
        public string Download()
        {
            try
            {
                LogtoFile.LogMessagePrint("Execute method Download");
                string settedurl = GetUrl;
                var client = new RestClient(settedurl)
                {
                    Authenticator = OAuth1Authenticator.ForProtectedResource(
                           QuickIA.ImportData.PrepareDataSet.BLL.Configuration.Configuration.GetApplicationConfig().ApiKeytwitter,
                           QuickIA.ImportData.PrepareDataSet.BLL.Configuration.Configuration.GetApplicationConfig().ApiSecretKeyTwitter,
                           QuickIA.ImportData.PrepareDataSet.BLL.Configuration.Configuration.GetApplicationConfig().TokenKeyTwitter,
                           QuickIA.ImportData.PrepareDataSet.BLL.Configuration.Configuration.GetApplicationConfig().TokenSecretKeyTwitter, RestSharp.Authenticators.OAuth.OAuthSignatureMethod.HmacSha1)
                };
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                var response = client.Execute(request);
                string test = response.Content;
                return test;
            }      
            catch (Exception ex)
            {
                LogtoFile.LogMessagePrint(ex.Message);
                return ex.Message;
            }
        }      
        public string url1;
        public string GetUrl
        {
            get
            {    
                if (string.IsNullOrEmpty(url1))
                    url1 = QuickIA.ImportData.PrepareDataSet.BLL.Configuration.Configuration.GetApplicationConfig().ApiUrlTwitter + "%24" + ticker + "%20" + ticker + "%20%24" + ticker + "&count=" + QuickIA.ImportData.PrepareDataSet.BLL.Configuration.Configuration.GetApplicationConfig().CounterTweet + "&lang=en" + "&tweet_mode=extended";
                return url1;
            }
            set
            {
                url1 = value;
            }
        }
    }
}
