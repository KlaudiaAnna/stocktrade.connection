﻿using System;

namespace QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation
{
    class InsideBar
    {
        public Context context;
        public InsideBar(Context c)
        {
            context = c;
        }
        public void Process_InsideBar()
        {
            for (int i = 0; i < context.Elements.Count; i++)
            {
                if (i == context.Elements.Count - 1)
                {
                    var values = context.Elements[i];
                    values.Values = string.Concat(values.Values, ",", "0");
                    return;
                }

                decimal high_today = context.Elements[i].High;
                decimal low_today = context.Elements[i].Low;
                decimal low_yestarday = context.Elements[i + 1].Low;
                decimal high_yestarday = context.Elements[i + 1].High;

                if (high_today < high_yestarday && low_today < low_yestarday)
                {
                    var values = context.Elements[i];
                    values.Values = String.Concat(values.Values, ",", "0.3");
                }
                else
                {
                    var values = context.Elements[i];
                    values.Values = String.Concat(values.Values, ",", "0");
                }


            }
        }
    }
}
