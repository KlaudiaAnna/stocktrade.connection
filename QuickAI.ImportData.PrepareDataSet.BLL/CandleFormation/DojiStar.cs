﻿using QuickIA.ImportData.PrepareDataSet.BLL;

namespace QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation
{
    class DojiStar
    {
        protected const decimal PERCENT = 0.4M;
        public Context context;
        public DojiStar(Context c)
        {
            context = c;
        }
        public void Process_Doji()
        {
            for (int i = 0; i < context.Elements.Count; i++)
            {
                var e = context.Elements[i];
                var data = e.Data;
                decimal open = context.Elements[i].Open;
                decimal close = context.Elements[i].Close;
                decimal high = context.Elements[i].High;
                decimal low = context.Elements[i].Low;

                decimal diff = high - low;
                decimal theshold = diff * PERCENT;
                decimal theshold_low = theshold + low;
                decimal theshold_high = high - theshold;

                if ((open > theshold_low && open < theshold_high) &&
                        (close < theshold_high && theshold_low < close))
                {
                    e.Values = string.Concat(e.Values, ",", "0.5");
                }
                else
                {
                    e.Values = string.Concat(e.Values, ",", "0");
                }
            }
        }
    }
}
