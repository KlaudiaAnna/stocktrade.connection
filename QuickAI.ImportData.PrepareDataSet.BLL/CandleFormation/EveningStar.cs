﻿namespace QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation
{   /* 1 */
    class EveningStar : FormationBase
    {
        public EveningStar(Context c)
        {
            context = c;
        }
        public void ProcessEveningStar()
        {
            for (int i = 0; i < context.Elements.Count; i++)
            {
                //  if (IsHossa(i))
                //{  
                if ((i == 0) || (i == context.Elements.Count - 1))
                {
                    var el = context.Elements[i];
                    el.Values = string.Concat(el.Values, ",", "0");
                }
                else
                {
                    var elem = context.Elements[i];
                    var data = elem.Data;
                    decimal candle1Open = context.Elements[i + 1].Open;
                    decimal candle1Close = context.Elements[i + 1].Close;
                    decimal candle3Close = context.Elements[i - 1].Close;
                    decimal candle3Open = context.Elements[i - 1].Open;
                    decimal candle2Open = context.Elements[i].Open;
                    decimal candle2Close = context.Elements[i].Close;

                    if ((candle1Open < candle1Close) && (candle3Open > candle3Close) // czy pierwsza swieca jest wzrostowa a trzecia spadkowa
                        && (candle1Open < candle2Close) && (candle2Close > candle3Open)) // luka pomiedzy pierwsza a druga druga  atrzecia
                    {
                        elem.Values = string.Concat(elem.Values, ",", "-1");
                    }
                    else
                    {
                        elem.Values = string.Concat(elem.Values, ",", "0");
                    }
                }
            }
        }
    }
}
