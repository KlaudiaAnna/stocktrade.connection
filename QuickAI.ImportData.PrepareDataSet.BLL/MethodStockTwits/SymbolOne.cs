﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using QuickAI.ImportData.PrepareDataSet.Interfaces.StockTwitsModel;
namespace QuickAI.ImportData.PrepareDataSet.BLL.MethodStockTwits
{
    public class SymbolOne
    {
        private const string expression = @"\$[a-zA-Z]+";
        private readonly string toreplace_forcheck = "*****";
        private readonly string ticker = string.Empty;
        public SymbolOne(string ticker)
        {
            this.ticker = ticker;
        }

        public void CheckSymbol(StockCollection el)
        {
            for (int i = el.Stockdata.Count - 1; i >= 0; i--)
            {
                const char s = '$';
                string Symbl = String.Concat(s, ticker);
                string text = el.Stockdata[i].Body;
                bool value = text.Contains(Symbl);

                if (value == true)
                {
                    var df = text.Replace(Symbl, toreplace_forcheck);
                    bool value2 = df.Contains(s);
                    if (value2 == true)
                    {
                        for (Match m = Regex.Match(df, expression); m.Success; m = m.NextMatch())
                        {
                            if (m.Success)
                            {
                                el.Stockdata.RemoveAt(i);
                                break;
                            }
                        }
                    }
                    else
                    {
                        df.Replace(toreplace_forcheck, Symbl);
                    }
                }
            }
        }
    }
}
