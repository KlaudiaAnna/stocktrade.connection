﻿using System;
using System.Collections.Generic;
using System.Globalization;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickAI.ImportData.PrepareDataSet.Interfaces.StockTwitsModel;
using QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel;
namespace QuickAI.ImportData.PrepareDataSet.BLL
{
    public class SettingToListTweetCollection
    {
        private readonly string ticker;
        public SettingToListTweetCollection(string ticker)
        {
            this.ticker = ticker;
        }
        public List<TweetCollectionCompleted> SettoListT(List<Tweet> list, List<TweetCollectionCompleted> obj)
        {
            foreach (var x in list)
            {
                DateTime s = DateTime.ParseExact(x.Created, "ddd MMM dd HH:mm:ss zzz yyyy", CultureInfo.InvariantCulture);
                obj.Add(new TweetCollectionCompleted
                {
                    Date = s,
                    IdUser = x.User.IDuser,
                    UserName = x.User.Name,
                    IdTweet = x.Id_str,
                    Text = x.Text,
                    Symbol = ticker,
                    DateInsertedDb = DateTime.Today.ToShortDateString(),
                    Human_Rating = String.Empty,
                    Neural_Rating = String.Empty,
                    Portal = "Twitter"
                });
            }
            return obj;
        }

        public List<TweetCollectionCompleted> SettoListS(List<StockData> respo, List<TweetCollectionCompleted> obj, StockCollection stockCollection)
        {
            foreach (var x in respo)
            {
                obj.Add(new TweetCollectionCompleted
                {
                    Text = x.Body,
                    Symbol = stockCollection.Symbol.Symbolticker,
                    UserName = x.User.NameUser,
                    IdUser = x.User.IdUser,
                    Date = x.Created_at,
                    IdTweet = x.Idbody,
                    DateInsertedDb = DateTime.Today.ToShortDateString(),
                    Human_Rating = string.Empty,
                    Neural_Rating = string.Empty,
                    Portal = "Stocktwits"
                });
            }
            return obj;
        }
    }
    
}
