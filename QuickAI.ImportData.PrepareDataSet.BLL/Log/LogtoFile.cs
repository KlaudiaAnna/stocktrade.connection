﻿using System;
using System.Configuration;
using System.IO;
using System.Threading;
namespace QuickAI.ImportData.PrepareDataSet.BLL.Log
{
    public static class LogtoFile
    {
        public static string NewPath = ConfigurationManager.AppSettings["PathtoLog"];
        public static void LogMessagePrint(string message)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(NewPath + "log_" + DateTime.Now.ToString("yyyy-dd-MM") + "_main.txt", true))
                {
                    sw.WriteLine(DateTime.Now + " " + message + "  ID thread:" + Thread.CurrentThread.ManagedThreadId);
                    sw.Close();
                }
            }
            catch { LogMessageError(message); }
        }
        public static void LogMessageError(string message)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(NewPath + "log_" + DateTime.Now.ToString("yyyy-dd-MM") + "_Catchmain.txt", true))
                {
                    sw.WriteLine(DateTime.Now + " " + message);
                    sw.Close();
                }
            }
            catch { }
        }
    }
}
