﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.BLL.Download;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.BLL.MethodTwitter;
using QuickAI.ImportData.PrepareDataSet.BLL.SavingProcess;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel;
namespace QuickAI.ImportData.PrepareDataSet.BLL.GeneralMethods
{
    public class TwitterGenerateData : IObtainData2
    {
        public List<TweetCollectionCompleted> GetCollectionComplete()
        {
            List<TweetCollectionCompleted> obj = new List<TweetCollectionCompleted>();
            var cfg = QuickIA.ImportData.PrepareDataSet.BLL.Configuration.Configuration.GetApplicationConfig();
            try
            {
                LogtoFile.LogMessagePrint("started twitter download");
                foreach (var ticker in cfg.Tickers)
                {
                    LogtoFile.LogMessagePrint(string.Format("started download for ticker:{0}", ticker.Name));

                    var p = new RequestToTwitter(ticker.Name);
                    var download = p.Download();
                    if (download != null)
                    {
                        string decode = WebUtility.HtmlDecode(download);
                        LogtoFile.LogMessagePrint("deserialized object");
                        Tweetdata tw = JsonConvert.DeserializeObject<Tweetdata>(decode);
                        LogtoFile.LogMessagePrint("check status of tweets");
                        CheckStatusofTweet.CheckStatus(tw.Statuses);
                        SaveTweets saveTweets = new SaveTweets();
                          saveTweets.SavetoTxt(tw.Statuses, "Twitter", ticker.Name);
                        LogtoFile.LogMessagePrint("count word in tweet");
                        CountWordinTweet.Words(tw);
                        var c = new Contains(ticker.Name);
                        LogtoFile.LogMessagePrint("Ticker check if it's one  ");
                        c.TickerOnlyOne(tw);
                        var list = tw.Statuses;
                        LogtoFile.LogMessagePrint("Add to new list item of tweet");

                        SettingToListTweetCollection collection = new SettingToListTweetCollection(ticker.Name);
                        var list_t = collection.SettoListT(list, obj);
                        list_t = obj;
                        LogtoFile.LogMessagePrint("Return list of tweets");
                    }
                    continue;
                }
                return obj;
            }
            catch (Exception ex)
            {
                LogtoFile.LogMessagePrint(ex.Message);
                return null;
            }
        }
    }
}
