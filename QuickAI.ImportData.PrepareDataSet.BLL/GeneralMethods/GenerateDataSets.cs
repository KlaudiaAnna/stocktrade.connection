﻿using System;
using System.Collections.Generic;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using System.IO;
using System.Configuration;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Ticker_Failed;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.BLL.SerializeJson;
namespace QuickAI.ImportData.PrepareDataSet.BLL.GeneralMethods
{
    public class GenerateDataSets : IObtainData
    {
        public bool GetDataSet()
        {
            List<TickersList> t = new List<TickersList>();
            try
            {
                LogtoFile.LogMessagePrint("Call proceess data!");
                ProccessData process = new ProccessData();
                ApplicationConfiguration cfg = QuickIA.ImportData.PrepareDataSet.BLL.Configuration.Configuration.GetApplicationConfig();
                List<TickerInfo> tickers = cfg.Tickers;
                List<TickerInfo> failedjs = new List<TickerInfo>();
                List<IndicatorInfo> indicators = cfg.Indicators;
                string path = ConfigurationManager.AppSettings["Json_Path"];
                string jpath = path + "TickerFailed_" + DateTime.Now.ToString("yyyy-dd-MM") + ".json";
                string pathtofailedjson = Path.Combine(path, jpath);

                if (File.Exists(pathtofailedjson))
                {
                    var jsondata = File.ReadAllText(pathtofailedjson);
                    List<Tickers_failed_elements> read = JsonConvert.DeserializeObject<List<Tickers_failed_elements>>(jsondata);

                    LogtoFile.LogMessagePrint(string.Format("all ticker failed is and indicators = {0},{1}", tickers.Count.ToString(), indicators.Count.ToString()));
                    foreach (var tickers_ in read)
                    {
                        foreach (var el in tickers_.Failed_tickers)
                        {
                            failedjs = new List<TickerInfo> { new TickerInfo { Name = el.Tickername, Index = el.Index } };
                        }
                    }
                    process.ProcessData(failedjs, indicators);

                    if (failedjs.Count != 0)
                    {
                        LogtoFile.LogMessagePrint(
                       string.Format("failed executed its contains, execute again ProcessData {0}", failedjs.Count));
                        var failed_again = process.ProcessData(failedjs, indicators);
                        if (failed_again == null)
                        {
                            File.Delete(pathtofailedjson);
                        }
                    }
                    LogtoFile.LogMessagePrint("Returned true of GetDataSet()");
                    return true;
                }
                else
                {
                    LogtoFile.LogMessagePrint(string.Format("all ticker and indicators = {0},{1}", tickers.Count.ToString(), indicators.Count.ToString()));
                    var failed = process.ProcessData(tickers, indicators);
                    if (failed.Count != 0)
                    {
                        LogtoFile.LogMessagePrint(
                            string.Format("failed executed its contains, execute again ProcessData {0}", failed.Count));
                        process.ProcessData(failed, indicators);
                    }
                    LogtoFile.LogMessagePrint("Returned true of GetDataSet()");
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogtoFile.LogMessagePrint(string.Format("Exception occured while running GetDataSet: {0}, {1}",
                 ex.Message, ex.InnerException));
                return false;
            }
        }
        public List<TickersList> TickerlistList()
        {
            try
            {
                ApplicationConfiguration cfg = QuickIA.ImportData.PrepareDataSet.BLL.Configuration.Configuration.GetApplicationConfig();
                List<TickersList> t = new List<TickersList>();
                foreach (var tickers in cfg.Tickers)
                {
                    t.Add(new TickersList
                    {
                        TickerName = tickers.Name
                    });
                }
                return t;
            }

            catch (Exception ex)
            {
                LogtoFile.LogMessagePrint(string.Format("Exception occured while running GetDataSet: {0}, {1}",
                ex.Message, ex.InnerException));
                return null;
            }
        }
    }
}



