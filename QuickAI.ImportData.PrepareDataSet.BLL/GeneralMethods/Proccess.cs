﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation;
using QuickAI.ImportData.PrepareDataSet.BLL.Download;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.BLL.NormalizeValues;
using QuickAI.ImportData.PrepareDataSet.BLL.SavingProcess;
using QuickAI.ImportData.PrepareDataSet.BLL.SerializeJson;
using QuickAI.ImportData.PrepareDataSet.BLL.Worker;
using QuickAI.ImportData.PrepareDataSet.BLL.Worker.currency;
using System.IO;

namespace QuickAI.ImportData.PrepareDataSet.BLL.GeneralMethods
{
    public class ProccessData
    {
        public List<TickerInfo> ProcessData(List<TickerInfo> tickers, List<IndicatorInfo> identificators)
        {
            List<TickerInfo> failedTickers = new List<TickerInfo>();
            try
            {
                foreach (TickerInfo ticker in tickers)
                {

                    Context context = new Context();
                    ReportSave report = new ReportSave(ticker.Name);
                    SerializeToJson toJson = new SerializeToJson(ticker.Name);
                    Ticker_Failed_json obj_ticker = new Ticker_Failed_json(ticker.Name);
                    QuickIaWebRequest webR = new QuickIaWebRequest(ticker.Name, "TIME_SERIES_DAILY");
                    string data = webR.Download();
                    TickerData tickerData = JsonConvert.DeserializeObject<TickerData>(data);

                    bool checkifcontainsinfailde = failedTickers.Contains(ticker);

                    if (tickerData.TimeSeriesData == null)
                    {
                        if (!checkifcontainsinfailde)
                        {
                            LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ticker.Name));
                            failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                            report.Reporting_notvalid();
                            continue;
                        }
                        continue;
                    }
                    tickerData.CleanseData();
                    tickerData.AddToContext(context, false);

                    /***********************************TICKER INDEX************************************************************************************/
                    webR = new QuickIaWebRequest(ticker.Index, "Time_Series_DAILY");
                    string idate = webR.Download();

                    tickerData = JsonConvert.DeserializeObject<TickerData>(idate);
                    if (tickerData.TimeSeriesData == null)
                    {
                        if (!checkifcontainsinfailde)
                        {
                            LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ticker.Index));
                            failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                            report.Reporting_notvalid();
                            continue;
                        }
                        continue;
                    }
                    tickerData.CleanseData();
                    tickerData.AddToContext(context, true);

                    /***********************************************************Indicators*************************************************************/
                    foreach (IndicatorInfo ii in identificators)
                    {
                        webR = new QuickIaWebRequest(ii.Name, "TIME_SERIES_DAILY");
                        if (ii.Name == "SMA")
                        {
                            webR.Url = IndicatorSMA.GetUrl(ticker.Name);
                            string smaData = webR.Download();

                            IndicatorSMA smaDataIndicator = JsonConvert.DeserializeObject<IndicatorSMA>(smaData);

                            if (smaDataIndicator.TimeSeriesData == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(
                                        string.Format("Add failed {0} to the list didn't execute:", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            smaDataIndicator.CleanseData();
                            smaDataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "AD")
                        {
                            webR.Url = IndicatorAD.GetUrl(ticker.Name);
                            string adData = webR.Download();

                            IndicatorAD ADDataIndicator = JsonConvert.DeserializeObject<IndicatorAD>(adData);
                            if (ADDataIndicator.TechnicalADdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            ADDataIndicator.CleanseData();
                            ADDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ADX")
                        {
                            webR.Url = IndicatorADX.GetUrl(ticker.Name);
                            string ADXata = webR.Download();
                            IndicatorADX ADXdataIndicator = JsonConvert.DeserializeObject<IndicatorADX>(ADXata);

                            if (ADXdataIndicator.TechnicalADXdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }

                            ADXdataIndicator.CleanseData();
                            ADXdataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "BBANDS")
                        {
                            webR.Url = IndicatorBBANDS.GetUrl(ticker.Name);
                            string BBANDSData = webR.Download();

                            IndicatorBBANDS BBANDSdataIndicator =
                                JsonConvert.DeserializeObject<IndicatorBBANDS>(BBANDSData);

                            if (BBANDSdataIndicator.TechnicalBBANDSdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            BBANDSdataIndicator.CleanseData();
                            BBANDSdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "T3")
                        {
                            webR.Url = IndicatorT3.GetUrl(ticker.Name);
                            string T3Data = webR.Download();

                            IndicatorT3 T3dataIndicator = JsonConvert.DeserializeObject<IndicatorT3>(T3Data);

                            if (T3dataIndicator.TechnicalT3data == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            T3dataIndicator.CleanseData();
                            T3dataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "MACD")
                        {
                            webR.Url = IndicatorMACD.GetUrl(ticker.Name);
                            string MACDData = webR.Download();

                            IndicatorMACD MACDdataIndicator =
                                JsonConvert.DeserializeObject<IndicatorMACD>(MACDData);

                            if (MACDdataIndicator.TechnicalMACDdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            MACDdataIndicator.CleanseData();
                            MACDdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "OBV")
                        {
                            webR.Url = IndicatotOBV.GetUrl(ticker.Name);
                            string OBVData = webR.Download();
                            IndicatotOBV OBVdataIndicator = JsonConvert.DeserializeObject<IndicatotOBV>(OBVData);

                            if (OBVdataIndicator.TechnicalOBVdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            OBVdataIndicator.CleanseData();
                            OBVdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "STOCH")
                        {
                            webR.Url = IndicatorStoch.GetUrl(ticker.Name);
                            string StochData = webR.Download();

                            IndicatorStoch StochdataIndicator =
                                JsonConvert.DeserializeObject<IndicatorStoch>(StochData);

                            if (StochdataIndicator.TechnicalSTOCHdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            StochdataIndicator.CleanseData();
                            StochdataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "RSI")
                        {
                            webR.Url = IndicatorRSI.GetUrl(ticker.Name);
                            string RSIData = webR.Download();

                            IndicatorRSI RSIdataIndicator = JsonConvert.DeserializeObject<IndicatorRSI>(RSIData);

                            if (RSIdataIndicator.TechnicalRSIdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            RSIdataIndicator.CleanseData();
                            RSIdataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "EMA")
                        {
                            webR.Url = IndicatorEMA.GetUrl(ticker.Name);
                            string EMAData = webR.Download();

                            IndicatorEMA EMAdataIndicator = JsonConvert.DeserializeObject<IndicatorEMA>(EMAData);

                            if (EMAdataIndicator.TechnicalEMAdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            EMAdataIndicator.CleanseData();
                            EMAdataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "CCI")
                        {
                            webR.Url = IndicatorCCI.GetUrl(ticker.Name);
                            string CCIData = webR.Download();
                            IndicatorCCI CCIdataIndicator = JsonConvert.DeserializeObject<IndicatorCCI>(CCIData);
                            if (CCIdataIndicator.TechnicalCCIdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            CCIdataIndicator.CleanseData();
                            CCIdataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "WMA")
                        {
                            webR.Url = IndicatorWMA.GetUrl(ticker.Name);
                            string WMAData = webR.Download();

                            IndicatorWMA WMAdataIndicator = JsonConvert.DeserializeObject<IndicatorWMA>(WMAData);

                            if (WMAdataIndicator.TechnicalWMAdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            WMAdataIndicator.CleanseData();
                            WMAdataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "AROON")
                        {
                            webR.Url = IndicatorAROON.GetUrl(ticker.Name);
                            string AroonData = webR.Download();

                            IndicatorAROON AroonDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorAROON>(AroonData);

                            if (AroonDataIndicator.TechnicalAROONdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }

                            AroonDataIndicator.CleanseData();
                            AroonDataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "MFI")
                        {
                            webR.Url = IndicatorMFI.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorMFI MFIDataIndicator = JsonConvert.DeserializeObject<IndicatorMFI>(Data);

                            if (MFIDataIndicator.TechnicalMFIdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            MFIDataIndicator.CleanseData();
                            MFIDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ULTOSC")
                        {
                            webR.Url = IndicatorULTOSC.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorULTOSC ULTOSCDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorULTOSC>(Data);

                            if (ULTOSCDataIndicator.TechnicalULTOSCdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            ULTOSCDataIndicator.CleanseData();
                            ULTOSCDataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "TRIX")
                        {
                            webR.Url = IndicatorTRIX.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorTRIX TRIXDataIndicator = JsonConvert.DeserializeObject<IndicatorTRIX>(Data);

                            if (TRIXDataIndicator.TechnicalTRIXdata == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            TRIXDataIndicator.CleanseData();
                            TRIXDataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "DX")
                        {
                            webR.Url = IndicatorDX.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorDX DXDataIndicator = JsonConvert.DeserializeObject<IndicatorDX>(Data);

                            if (DXDataIndicator.TechnicalDX == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            DXDataIndicator.CleanseData();
                            DXDataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "WILLR")
                        {
                            webR.Url = IndicatorWILLR.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorWILLR WILLRDataIndicator = JsonConvert.DeserializeObject<IndicatorWILLR>(Data);

                            if (WILLRDataIndicator.TechnicalWILLR == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            WILLRDataIndicator.CleanseData();
                            WILLRDataIndicator.AddToContext(context);


                        }

                        if (ii.Name == "ADXR")
                        {
                            webR.Url = IndicatorADXR.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorADXR ADXRDataIndicator = JsonConvert.DeserializeObject<IndicatorADXR>(Data);

                            if (ADXRDataIndicator.TechnicalADXR == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            ADXRDataIndicator.CleanseData();
                            ADXRDataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "PPO")
                        {
                            webR.Url = IndicatorPPO.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorPPO PPODataIndicator = JsonConvert.DeserializeObject<IndicatorPPO>(Data);

                            if (PPODataIndicator.TechnicalPPO == null)
                            {
                                if (!checkifcontainsinfailde)
                                {

                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            PPODataIndicator.CleanseData();
                            PPODataIndicator.AddToContext(context);

                        }

                        if (ii.Name == "MOM")
                        {
                            webR.Url = IndicatorMOM.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorMOM MOMDataIndicator = JsonConvert.DeserializeObject<IndicatorMOM>(Data);

                            if (MOMDataIndicator.TechnicalMOM == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            MOMDataIndicator.CleanseData();
                            MOMDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "BOP")
                        {
                            webR.Url = IndicatorBOP.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorBOP BOPDataIndicator = JsonConvert.DeserializeObject<IndicatorBOP>(Data);

                            if (BOPDataIndicator.TechnicalBOP == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            BOPDataIndicator.CleanseData();
                            BOPDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "CMO")
                        {
                            webR.Url = IndicatorCMO.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            IndicatorCMO CMODataIndicator = JsonConvert.DeserializeObject<IndicatorCMO>(Data);
                            if (CMODataIndicator.TechnicalCMO == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            CMODataIndicator.CleanseData();
                            CMODataIndicator.AddToContext(context);

                        }
                        if (ii.Name == "ROC")
                        {
                            webR.Url = IndicatorROC.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            IndicatorROC ROCDataIndicator = JsonConvert.DeserializeObject<IndicatorROC>(Data);
                            if (ROCDataIndicator.TechnicalROC == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            ROCDataIndicator.CleanseData();
                            ROCDataIndicator.AddToContext(context);

                        }
                        if (ii.Name == "PLUS_DI")
                        {
                            webR.Url = IndicatorPlusDi.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorPlusDi PlusDiDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorPlusDi>(Data);

                            if (PlusDiDataIndicator.TechnicalPlusDi == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            PlusDiDataIndicator.CleanseData();
                            PlusDiDataIndicator.AddToContext(context);

                        }
                        if (ii.Name == "MINUS_DM")
                        {
                            webR.Url = IndicatorMINUS_DM.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorMINUS_DM MINUSDMDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorMINUS_DM>(Data);

                            if (MINUSDMDataIndicator.TechnicalMINUS_DM == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            MINUSDMDataIndicator.CleanseData();
                            MINUSDMDataIndicator.AddToContext(context);

                        }
                        if (ii.Name == "ADOSC")
                        {
                            webR.Url = IndicatorADOSC.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorADOSC ADOSCDataIndicator = JsonConvert.DeserializeObject<IndicatorADOSC>(Data);

                            if (ADOSCDataIndicator.TechnicalADOSC == null)
                            {
                                if (!checkifcontainsinfailde)
                                {

                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;

                            }
                            ADOSCDataIndicator.CleanseData();
                            ADOSCDataIndicator.AddToContext(context);
                        }
                        if (ii.Name == "HT_TRENDLINE")
                        {
                            webR.Url = IndicatorHT_TRENDLINE.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorHT_TRENDLINE HTDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorHT_TRENDLINE>(Data);

                            if (HTDataIndicator.TechnicalHT == null)
                            {
                                if (!checkifcontainsinfailde)
                                {
                                    LogtoFile.LogMessagePrint(string.Format("Add failed {0} to the list", ii.Name));
                                    failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                                    report.Reporting_notvalid();
                                    break;
                                }
                                break;
                            }
                            HTDataIndicator.CleanseData();
                            HTDataIndicator.AddToContext(context);
                        }
                    }
                    /*******************************************volume_divided ********************************************************************************/

                    LogtoFile.LogMessagePrint("Normalize for volume");
                    NormalizeVolume v = new NormalizeVolume(context);
                    v.VolumeCaluculate();

                    LogtoFile.LogMessagePrint("Normalize for volume_index");
                    NormalizeVolumeIndex vindex = new NormalizeVolumeIndex(context);
                    vindex.VolumeIndexCaluculate();

                    LogtoFile.LogMessagePrint("Normalize for AD");
                    NormalizeAD ad = new NormalizeAD(context);
                    ad.ADCaluculate();

                    LogtoFile.LogMessagePrint("Normalize for adosc");
                    NormalizeADOSC n_adosc = new NormalizeADOSC(context);
                    n_adosc.ADOSCaluculate();

                    LogtoFile.LogMessagePrint("Normalize for OBV");
                    NormalizeOBV obv = new NormalizeOBV(context);
                    obv.OBVcalculate();

                    LogtoFile.LogMessagePrint(string.Format("set seasonal"));
                    SeasonalTime seasonal = new SeasonalTime(context);
                    seasonal.Add_Monts_Day();

                    /**********************************************call formations*****************************************************************************/
                    if (context.Elements.Any(x => x.Open != 0 && x.Volume != 0 && x.Close != 0 && x.High != 0 && x.Low != 0))
                    {
                        LogtoFile.LogMessagePrint(string.Format("call formation for {0}", ticker.Name));
                        Hammer h = new Hammer(context);
                        h.ProcessHammer();

                        HangedMan hanged = new HangedMan(context);
                        hanged.ProcessHangedMan();

                        Hossa_h hossa_H = new Hossa_h(context);
                        hossa_H.Process_Hossa_h();

                        Bessa_b bessa_b = new Bessa_b(context);
                        bessa_b.Process_Bessa_b();

                        InsideBar ib = new InsideBar(context);
                        ib.Process_InsideBar();

                        DojiStar ds = new DojiStar(context);
                        ds.Process_Doji();

                        MorningStar ms = new MorningStar(context);
                        ms.ProcessMorningStar();

                        EveningStar ev = new EveningStar(context);
                        ev.ProcessEveningStar();
                    }
                    else
                    {
                        if (!checkifcontainsinfailde)
                        {
                            LogtoFile.LogMessagePrint("Cannot call formation: context is null");
                            failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                            report.Reporting_notvalid();
                            continue;
                        }
                        continue;

                    }
                    /**************************************Currency******************************************************************************************************/
                    LogtoFile.LogMessagePrint(string.Format("get currency {0}", ticker.Name));
                    //   EUR_USD
                    webR.Url = EUR_USD_currency_method.GetUrl();
                    string EUR_USD = webR.Download();

                    EUR_USD_currency_method EUR_USD_CUR =
                        JsonConvert.DeserializeObject<EUR_USD_currency_method>(EUR_USD);
                    if (EUR_USD_CUR.TechnicalEUR_USD == null)
                    {
                        if (!checkifcontainsinfailde)
                        {
                            LogtoFile.LogMessagePrint(string.Format("Could not get the currency"));
                            failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                            report.Reporting_notvalid();
                            continue;
                        }
                        continue;
                    }
                    if (EUR_USD_CUR.TechnicalEUR_USD != null)
                    {
                        EUR_USD_CUR.CleanseData();
                        EUR_USD_CUR.AddToContext(context);
                    }
                    //   USD_AUD
                    webR.Url = USD_AUD_currency_method.GetUrl();
                    string USD_AUD = webR.Download();
                    USD_AUD_currency_method USD_AUD_CUR =
                        JsonConvert.DeserializeObject<USD_AUD_currency_method>(USD_AUD);
                    if (USD_AUD_CUR.TechnicalUSD_AUD == null)
                    {
                        if (!checkifcontainsinfailde)
                        {
                            LogtoFile.LogMessagePrint(string.Format("Could not get the currency"));
                            failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                            report.Reporting_notvalid();
                            continue;
                        }
                        continue;
                    }

                    if (USD_AUD_CUR.TechnicalUSD_AUD != null)
                    {
                        USD_AUD_CUR.CleanseData();
                        USD_AUD_CUR.AddToContext(context);
                    }
                    // USD_GBP
                    webR.Url = USD_GBP_currency_method.GetUrl();
                    string USD_GBP = webR.Download();

                    USD_GBP_currency_method USD_GBP_CUR =
                        JsonConvert.DeserializeObject<USD_GBP_currency_method>(USD_GBP);
                    if (USD_GBP_CUR.TechnicalUSD_GBP == null)
                    {
                        if (!checkifcontainsinfailde)
                        {
                            LogtoFile.LogMessagePrint(string.Format("Could not get the currency"));
                            failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                            report.Reporting_notvalid();
                            continue;
                        }
                        continue;
                    }
                    if (USD_GBP_CUR.TechnicalUSD_GBP != null)
                    {
                        USD_GBP_CUR.CleanseData();
                        USD_GBP_CUR.AddToContext(context);
                    }
                    //     USD_JPY
                    webR.Url = USD_JPY_currency_method.GetUrl();
                    string USD_JPY = webR.Download();
                    USD_JPY_currency_method USD_JPY_CUR =
                        JsonConvert.DeserializeObject<USD_JPY_currency_method>(USD_JPY);
                    if (USD_JPY_CUR.TechnicalUSD_JPY == null)
                    {
                        if (!checkifcontainsinfailde)
                        {
                            LogtoFile.LogMessagePrint(string.Format("Could not get the currency"));
                            failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                            report.Reporting_notvalid();
                            continue;
                        }
                        continue;
                    }
                    if (USD_JPY_CUR.TechnicalUSD_JPY != null)
                    {   
                        USD_JPY_CUR.CleanseData();
                        USD_JPY_CUR.AddToContext(context);
                    }
                    /**********************************************save to files***************************************************************************************************************/
                    if (checkifcontainsinfailde)
                    {
                        LogtoFile.LogMessagePrint("Not save get next, not all identicators were download!");
                        report.Reporting_notvalid();
                        continue;
                    }
                    else
                    {
                        SaveCSV save = new SaveCSV(context);
                        bool original = save.Save_Original(ticker.Name);
                        bool classifier = save.Save_Classifier(ticker.Name);
                        bool reversedf = save.Save_Reversed(ticker.Name);
                        report.Reporting(original, reversedf, classifier);

                        if (checkifcontainsinfailde)
                        {
                            LogtoFile.LogMessagePrint("list contains failed ticker");
                            report.Reporting_notvalid();
                            obj_ticker.SetoJson();
                        }
                        toJson.SetoJson(original, reversedf, classifier);
                    }
                }
                LogtoFile.LogMessagePrint(string.Format("Proccess data returned to generate DataSets failed tickers : {0} ", failedTickers.Count));
                return failedTickers;
            }
            catch (Exception ex)
            {
                LogtoFile.LogMessagePrint(string.Format("Exception occured while running ProcessData: {0}, {1}",
                ex.Message, ex.InnerException));
                return null;
            }
        }
    }
}

