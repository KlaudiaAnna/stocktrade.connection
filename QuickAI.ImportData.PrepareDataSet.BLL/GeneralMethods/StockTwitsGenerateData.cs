﻿using System;
using System.Collections.Generic;
using QuickAI.ImportData.PrepareDataSet.BLL.Download;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.BLL.MethodStockTwits;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
namespace QuickAI.ImportData.PrepareDataSet.BLL.GeneralMethods
{
    public class StockTwitsGenerateData : IObtainData2
    {
        private readonly long _idSTock;
        public StockTwitsGenerateData(long idSTock)
        {
            _idSTock = idSTock;
        }
        public List<TweetCollectionCompleted> GetCollectionComplete()
        {
            try
            {
                LogtoFile.LogMessagePrint("Start Download from StockTwits");
                List<TweetCollectionCompleted> objecList = new List<TweetCollectionCompleted>();
                var configuration = Configuration.GetApplicationConfig();
                foreach (var ticker in configuration.Tickers)
                {
                    LogtoFile.LogMessagePrint(string.Format("Reguest to ticker: ", ticker.Name));
                    RequestToStocktwits request = new RequestToStocktwits(ticker.Name, _idSTock);
                    var respo = request.Request();
                    if (respo != null)
                    {
                        LogtoFile.LogMessagePrint("Count word in stock tweet ");
                        CountWordStock.CountWordStocker(respo);
                        LogtoFile.LogMessagePrint("Check symbol in tweet ");
                        SymbolOne symbol = new SymbolOne(ticker.Name);
                        symbol.CheckSymbol(respo);
                        LogtoFile.LogMessagePrint("Add to list");
                    }
                    SettingToListTweetCollection collection = new SettingToListTweetCollection(ticker.Name);
                    var list = collection.SettoListS(respo.Stockdata, objecList, respo);
                    list = objecList;
                }
                LogtoFile.LogMessagePrint("Return list");
                return objecList;
            }
            catch (Exception e)
            {
                LogtoFile.LogMessagePrint(e.Message);
                return null;
            }
        }
    }
}
