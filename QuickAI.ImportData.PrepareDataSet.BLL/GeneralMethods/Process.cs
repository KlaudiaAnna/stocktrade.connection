﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation;
using QuickAI.ImportData.PrepareDataSet.BLL.Download;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.BLL.NormalizeValues;
using QuickAI.ImportData.PrepareDataSet.BLL.Worker;
using QuickAI.ImportData.PrepareDataSet.BLL.Worker.currency;
namespace QuickAI.ImportData.PrepareDataSet.BLL.GeneralMethods
{
    public class Process
    {

        public Process()
        { }

        public List<TickerInfo> failedTickers = new List<TickerInfo>();
        public List<IndicatorInfo> failedIndentificator = new List<IndicatorInfo>();

        public Tuple<List<TickerInfo>, List<IndicatorInfo>> ProcessData(List<TickerInfo> tickers, List<IndicatorInfo> identificators)
        {
            try
            {
                foreach (TickerInfo ticker in tickers)
                {
                    LogtoFile logtoFile = new LogtoFile();
                    logtoFile.LogMessagePrint(string.Format("All tickers,{0}", tickers.Count));
                    Context context = new Context();
                    QuickIaWebRequest webR = new QuickIaWebRequest(ticker.Name, "TIME_SERIES_DAILY");
                    string data = webR.Download();
                    if (data == null)
                    {
                        failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                        continue;
                    }
                    TickerData tickerData = JsonConvert.DeserializeObject<TickerData>(data);
                    tickerData.CleanseData();

                    if (tickerData.TimeSeriesData == null)
                    {
                        logtoFile.LogMessagePrint(string.Format("Could not get the ticker date for {0}!", ticker.Name));
                        failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                        continue;
                    }
                    tickerData.AddToContext(context, false);

                    /***********************************TICKER INDEX************************************************************************************/

                    webR = new QuickIaWebRequest(ticker.Index, "Time_Series_DAILY");

                    string idate = webR.Download();

                    if (idate == null)
                    {
                        failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                        continue;
                    }

                    tickerData = JsonConvert.DeserializeObject<TickerData>(idate);
                    tickerData.CleanseData();
                    if (tickerData.TimeSeriesData == null)
                    {
                        logtoFile.LogMessagePrint(string.Format("Index is empty in {0}", ticker.Name));
                        failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                        continue;
                    }
                    tickerData.AddToContext(context, true);

                    /***********************************************************Indicators*************************************************************/
                    foreach (IndicatorInfo ii in identificators)
                    {
                        logtoFile.LogMessagePrint(string.Format("get indicators: {0}, all is {1}", ii.Name, identificators.Count));

                        webR = new QuickIaWebRequest(ii.Name, "TIME_SERIES_DAILY");

                        if (ii.Name == "SMA")
                        {
                            webR.Url = IndicatorSMA.GetUrl(ticker.Name);
                            string smaData = webR.Download();

                            if (smaData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorSMA smaDataIndicator = JsonConvert.DeserializeObject<IndicatorSMA>(smaData);
                            smaDataIndicator.CleanseData();

                            if (smaDataIndicator.TimeSeriesData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            smaDataIndicator.AddToContext(context);
                        }
                        if (ii.Name == "AD")
                        {
                            webR.Url = IndicatorAD.GetUrl(ticker.Name);
                            string adData = webR.Download();
                            if (adData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorAD ADDataIndicator = JsonConvert.DeserializeObject<IndicatorAD>(adData);
                            ADDataIndicator.CleanseData();

                            if (ADDataIndicator.TechnicalADdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            ADDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ADX")
                        {
                            webR.Url = IndicatorADX.GetUrl(ticker.Name);
                            string ADXata = webR.Download();
                            if (ADXata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorADX ADXdataIndicator = JsonConvert.DeserializeObject<IndicatorADX>(ADXata);
                            ADXdataIndicator.CleanseData();

                            if (ADXdataIndicator.TechnicalADXdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            ADXdataIndicator.AddToContext(context);
                        }
                        if (ii.Name == "BBANDS")
                        {
                            webR.Url = IndicatorBBANDS.GetUrl(ticker.Name);
                            string BBANDSData = webR.Download();
                            if (BBANDSData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorBBANDS BBANDSdataIndicator =
                                JsonConvert.DeserializeObject<IndicatorBBANDS>(BBANDSData);
                            BBANDSdataIndicator.CleanseData();

                            if (BBANDSdataIndicator.TechnicalBBANDSdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            BBANDSdataIndicator.AddToContext(context);
                        }
                        if (ii.Name == "T3")
                        {
                            webR.Url = IndicatorT3.GetUrl(ticker.Name);
                            string T3Data = webR.Download();
                            if (T3Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorT3 T3dataIndicator = JsonConvert.DeserializeObject<IndicatorT3>(T3Data);
                            T3dataIndicator.CleanseData();

                            if (T3dataIndicator.TechnicalT3data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            T3dataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "MACD")
                        {
                            webR.Url = IndicatorMACD.GetUrl(ticker.Name);
                            string MACDData = webR.Download();
                            if (MACDData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorMACD MACDdataIndicator =
                                JsonConvert.DeserializeObject<IndicatorMACD>(MACDData);
                            MACDdataIndicator.CleanseData();

                            if (MACDdataIndicator.TechnicalMACDdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            MACDdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "OBV")
                        {
                            webR.Url = IndicatotOBV.GetUrl(ticker.Name);
                            string OBVData = webR.Download();
                            if (OBVData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatotOBV OBVdataIndicator = JsonConvert.DeserializeObject<IndicatotOBV>(OBVData);
                            OBVdataIndicator.CleanseData();

                            if (OBVdataIndicator.TechnicalOBVdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            OBVdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "STOCH")
                        {
                            webR.Url = IndicatorStoch.GetUrl(ticker.Name);
                            string StochData = webR.Download();
                            if (StochData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorStoch StochdataIndicator =
                                JsonConvert.DeserializeObject<IndicatorStoch>(StochData);
                            StochdataIndicator.CleanseData();

                            if (StochdataIndicator.TechnicalSTOCHdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            StochdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "RSI")
                        {
                            webR.Url = IndicatorRSI.GetUrl(ticker.Name);
                            string RSIData = webR.Download();
                            if (RSIData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorRSI RSIdataIndicator = JsonConvert.DeserializeObject<IndicatorRSI>(RSIData);
                            RSIdataIndicator.CleanseData();

                            if (RSIdataIndicator.TechnicalRSIdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            RSIdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "EMA")
                        {
                            webR.Url = IndicatorEMA.GetUrl(ticker.Name);
                            string EMAData = webR.Download();
                            if (EMAData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorEMA EMAdataIndicator = JsonConvert.DeserializeObject<IndicatorEMA>(EMAData);
                            EMAdataIndicator.CleanseData();

                            if (EMAdataIndicator.TechnicalEMAdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            EMAdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "CCI")
                        {
                            webR.Url = IndicatorCCI.GetUrl(ticker.Name);
                            string CCIData = webR.Download();
                            if (CCIData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorCCI CCIdataIndicator = JsonConvert.DeserializeObject<IndicatorCCI>(CCIData);
                            CCIdataIndicator.CleanseData();

                            if (CCIdataIndicator.TechnicalCCIdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            CCIdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "WMA")
                        {
                            webR.Url = IndicatorWMA.GetUrl(ticker.Name);
                            string WMAData = webR.Download();
                            if (WMAData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorWMA WMAdataIndicator = JsonConvert.DeserializeObject<IndicatorWMA>(WMAData);
                            WMAdataIndicator.CleanseData();

                            if (WMAdataIndicator.TechnicalWMAdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            WMAdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "AROON")
                        {
                            webR.Url = IndicatorAROON.GetUrl(ticker.Name);
                            string AroonData = webR.Download();
                            if (AroonData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorAROON AroonDataIndicator =
                            JsonConvert.DeserializeObject<IndicatorAROON>(AroonData);
                            AroonDataIndicator.CleanseData();

                            if (AroonDataIndicator.TechnicalAROONdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            AroonDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "MFI")
                        {
                            webR.Url = IndicatorMFI.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorMFI MFIDataIndicator = JsonConvert.DeserializeObject<IndicatorMFI>(Data);
                            MFIDataIndicator.CleanseData();

                            if (MFIDataIndicator.TechnicalMFIdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            MFIDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ULTOSC")
                        {
                            webR.Url = IndicatorULTOSC.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorULTOSC ULTOSCDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorULTOSC>(Data);
                            ULTOSCDataIndicator.CleanseData();

                            if (ULTOSCDataIndicator.TechnicalULTOSCdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            ULTOSCDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "TRIX")
                        {
                            webR.Url = IndicatorTRIX.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorTRIX TRIXDataIndicator = JsonConvert.DeserializeObject<IndicatorTRIX>(Data);
                            TRIXDataIndicator.CleanseData();

                            if (TRIXDataIndicator.TechnicalTRIXdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            TRIXDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "DX")
                        {
                            webR.Url = IndicatorDX.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorDX DXDataIndicator = JsonConvert.DeserializeObject<IndicatorDX>(Data);
                            DXDataIndicator.CleanseData();

                            if (DXDataIndicator.TechnicalDX == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            DXDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "WILLR")
                        {
                            webR.Url = IndicatorWILLR.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorWILLR WILLRDataIndicator = JsonConvert.DeserializeObject<IndicatorWILLR>(Data);
                            WILLRDataIndicator.CleanseData();

                            if (WILLRDataIndicator.TechnicalWILLR == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            WILLRDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ADXR")
                        {
                            webR.Url = IndicatorADXR.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorADXR ADXRDataIndicator = JsonConvert.DeserializeObject<IndicatorADXR>(Data);
                            ADXRDataIndicator.CleanseData();

                            if (ADXRDataIndicator.TechnicalADXR == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            ADXRDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "PPO")
                        {
                            webR.Url = IndicatorPPO.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorPPO PPODataIndicator = JsonConvert.DeserializeObject<IndicatorPPO>(Data);
                            PPODataIndicator.CleanseData();

                            if (PPODataIndicator.TechnicalPPO == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            PPODataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "MOM")
                        {
                            webR.Url = IndicatorMOM.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorMOM MOMDataIndicator = JsonConvert.DeserializeObject<IndicatorMOM>(Data);
                            MOMDataIndicator.CleanseData();

                            if (MOMDataIndicator.TechnicalMOM == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            MOMDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "BOP")
                        {
                            webR.Url = IndicatorBOP.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorBOP BOPDataIndicator = JsonConvert.DeserializeObject<IndicatorBOP>(Data);
                            BOPDataIndicator.CleanseData();

                            if (BOPDataIndicator.TechnicalBOP == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            BOPDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "CMO")
                        {
                            webR.Url = IndicatorCMO.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorCMO CMODataIndicator = JsonConvert.DeserializeObject<IndicatorCMO>(Data);
                            CMODataIndicator.CleanseData();

                            if (CMODataIndicator.TechnicalCMO == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            CMODataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ROC")
                        {
                            webR.Url = IndicatorROC.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorROC ROCDataIndicator = JsonConvert.DeserializeObject<IndicatorROC>(Data);
                            ROCDataIndicator.CleanseData();

                            if (ROCDataIndicator.TechnicalROC == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }
                            ROCDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "PLUS_DI")
                        {
                            webR.Url = IndicatorPlusDi.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorPlusDi PlusDiDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorPlusDi>(Data);
                            PlusDiDataIndicator.CleanseData();

                            if (PlusDiDataIndicator.TechnicalPlusDi == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }
                            PlusDiDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "MINUS_DM")
                        {
                            webR.Url = IndicatorMINUS_DM.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorMINUS_DM MINUSDMDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorMINUS_DM>(Data);
                            MINUSDMDataIndicator.CleanseData();

                            if (MINUSDMDataIndicator.TechnicalMINUS_DM == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }
                            MINUSDMDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ADOSC")
                        {
                            webR.Url = IndicatorADOSC.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorADOSC ADOSCDataIndicator = JsonConvert.DeserializeObject<IndicatorADOSC>(Data);
                            ADOSCDataIndicator.CleanseData();

                            if (ADOSCDataIndicator.TechnicalADOSC == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }
                            ADOSCDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "HT_TRENDLINE")
                        {
                            webR.Url = IndicatorHT_TRENDLINE.GetUrl(ticker.Name);
                            string Data = webR.Download();
                            if (Data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }
                            IndicatorHT_TRENDLINE HTDataIndicator = JsonConvert.DeserializeObject<IndicatorHT_TRENDLINE>(Data);
                            HTDataIndicator.CleanseData();

                            if (HTDataIndicator.TechnicalHT == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                logtoFile.LogMessagePrint(string.Format("Indeticator {0} is null. It'll be add to new list failed", ii.Name));
                                continue;
                            }

                            HTDataIndicator.AddToContext(context);
                        }
                    }
                    /*******************************************volume_divided ********************************************************************************/


                    NormalizeADOSC adosc = new NormalizeADOSC(context);
                    adosc.ADOSCaluculate();

                    NormalizeVolume v = new NormalizeVolume(context); // normalize volume
                    v.VolumeCaluculate();

                    NormalizeVolumeIndex vindex = new NormalizeVolumeIndex(context); // NORMALIZE INDEX VOLUME
                    vindex.VolumeIndexCaluculate();

                    NormalizeAD ad = new NormalizeAD(context);
                    ad.ADCaluculate();

                    NormalizeOBV obv = new NormalizeOBV(context);
                    obv.OBVcalculate();

                    /**********************************************Add day and month************************************************************/

                    SeasonalTime seasonal = new SeasonalTime(context);
                    seasonal.Add_Monts_Day();

                    /**********************************************call formations*****************************************************************************/

                    Hammer h = new Hammer(context);
                    h.ProcessHammer();

                    HangedMan hanged = new HangedMan(context);
                    hanged.ProcessHangedMan();

                    Hossa_h hossa_H = new Hossa_h(context);
                    hossa_H.Process_Hossa_h();

                    Bessa_b bessa_b = new Bessa_b(context);
                    bessa_b.Process_Bessa_b();

                    InsideBar ib = new InsideBar(context);
                    ib.Process_InsideBar();

                    DojiStar ds = new DojiStar(context);
                    ds.Process_Doji();

                    MorningStar ms = new MorningStar(context);
                    ms.ProcessMorningStar();

                    EveningStar ev = new EveningStar(context);
                    ev.ProcessEveningStar();

                    /**************************************Currency**************************************************************************************/

                    // EUR_USD
                    webR.Url = EUR_USD_currency_method.GetUrl();
                    string EUR_USD = webR.Download();

                    EUR_USD_currency_method EUR_USD_CUR =
                        JsonConvert.DeserializeObject<EUR_USD_currency_method>(EUR_USD);
                    EUR_USD_CUR.CleanseData();
                    if (EUR_USD_CUR.TechnicalEUR_USD == null)
                    {
                        logtoFile.LogMessagePrint("Currency Indeticator is null {0}!");
                        continue;
                    }

                    EUR_USD_CUR.AddToContext(context);

                    //USD_AUD
                    webR.Url = USD_AUD_currency_method.GetUrl();
                    string USD_AUD = webR.Download();

                    USD_AUD_currency_method USD_AUD_CUR =
                        JsonConvert.DeserializeObject<USD_AUD_currency_method>(USD_AUD);
                    USD_AUD_CUR.CleanseData();
                    if (USD_AUD_CUR.TechnicalUSD_AUD == null)
                    {
                        logtoFile.LogMessagePrint("Currency Indeticator is null {0}!");
                        continue;
                    }

                    USD_AUD_CUR.AddToContext(context);

                    //USD_GBP

                    //webR.Url = USD_GBP_currency_method.GetUrl();

                    //string USD_GBP = webR.Download();

                    //USD_GBP_currency_method USD_GBP_CUR =
                    //    JsonConvert.DeserializeObject<USD_GBP_currency_method>(USD_GBP);
                    //USD_GBP_CUR.CleanseData();
                    //if (USD_GBP_CUR.TechnicalUSD_GBP == null)
                    //{
                    //    logtoFile.LogMessagePrint("Currency Indeticator is null {0}!");
                    //    continue;
                    //}

                    //USD_GBP_CUR.AddToContext(context);

                    //USD_JPY
                    webR.Url = USD_JPY_currency_method.GetUrl();
                    string USD_JPY = webR.Download();
                    USD_JPY_currency_method USD_JPY_CUR =
                    JsonConvert.DeserializeObject<USD_JPY_currency_method>(USD_JPY);
                    USD_JPY_CUR.CleanseData();
                    if (USD_JPY_CUR.TechnicalUSD_JPY == null)
                    {
                        logtoFile.LogMessagePrint("Currency Indeticator is null {0}!");
                        continue;
                    }
                    USD_JPY_CUR.AddToContext(context);

                    /**********************************************save to files***************************************************************************************************************/
                    logtoFile.LogMessagePrint(string.Format("start to saving process {0}", ticker.Name));

                    context.SaveToFile(ticker.Name);
                    context.Save_Reversed(ticker.Name);
                    context.Save_Classifier(ticker.Name);
                }

                return Tuple.Create(failedTickers, failedIndentificator);
            }
            catch (Exception ex)
            {
                LogtoFile logtoFile = new LogtoFile();
                logtoFile.LogMessagePrint(string.Format("Exception occured while running Process: {0}, {1}", ex.Message, ex.InnerException));
                return null;
            }
        }
    }

}
