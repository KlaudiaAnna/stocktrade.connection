﻿using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.WILLR;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
   public class IndicatorWILLR
    {
        public IndicatorWILLR()
        { }

        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataWILLR MetadataWILLR { get; set; }
        [JsonProperty(PropertyName = "Technical Analysis: WILLR")]
        public Dictionary<string, TechnicalWILLR> TechnicalWILLR { get; set; }

        public static string GetUrl(string ticker)
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "WILLR&symbol=", ticker,
                "&interval=daily&time_period=10&series_type=close");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalWILLR)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalWILLR[timeSeria.Key];
                element.Values = string.Concat(element.Values, ",", values.WILLR);
            }
        }
        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;


            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalWILLR.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalWILLR.Remove(toRemove[i].Key);
            }
        }



    }
}
