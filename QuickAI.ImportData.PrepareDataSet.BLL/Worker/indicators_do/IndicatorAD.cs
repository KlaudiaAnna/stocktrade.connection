﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.AD;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
    public class IndicatorAD
    {
        public IndicatorAD()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataAD MetadataAD { get; set; }
        [JsonProperty(PropertyName = "Technical Analysis: Chaikin A/D")]
        public Dictionary<string, TechnicalAD> TechnicalADdata { get; set; }

        public static string GetUrl(string ticker)
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "AD&symbol=", ticker,
                "&interval=daily&time_period=10&series_type=close");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalADdata)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalADdata[timeSeria.Key];
                System.Globalization.NumberFormatInfo numinf = new System.Globalization.NumberFormatInfo();
                numinf.NumberDecimalSeparator = ".";

                element.AD = decimal.Parse(values.AD, numinf);
            }
        }
        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalADdata.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalADdata.Remove(toRemove[i].Key);
            }
        }
    }
}


