﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.WMA;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
    public class IndicatorWMA
    {
        public IndicatorWMA()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataWMA MetadataWMA { get; set; }
        [JsonProperty(PropertyName = "Technical Analysis: WMA")]
        public Dictionary<string, TechnicalWMA> TechnicalWMAdata { get; set; }

        public static string GetUrl(string ticker)
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "WMA&symbol=", ticker,
                "&interval=daily&time_period=10&series_type=close");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalWMAdata)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalWMAdata[timeSeria.Key];
                element.Values = string.Concat(element.Values, ",", values.WMA);

            }
        }
        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalWMAdata.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalWMAdata.Remove(toRemove[i].Key);
            }
        }
    }
}
