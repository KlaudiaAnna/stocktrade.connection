﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.TickerData_model;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
    public class TickerData
    {
        internal static IEnumerable<object> Timeseries;

        [JsonProperty(PropertyName = "Meta Data")]
        public Metadata Metadata { get; set; }

        [JsonProperty(PropertyName = "Time Series (Daily)")]
        public Dictionary<string, TimeSeries> TimeSeriesData { get; set; }


        public static string GetUrl(string ticker)
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "TIME_SERIES_DAILY&symbol=", ticker,
                "apikey=", Configuration.GetApplicationConfig());
        }

        public void AddToContext(Context context, bool isIndex = false)
        {
            LogtoFile.LogMessagePrint("Fill context value");

            foreach (var timeSeria in TimeSeriesData)
            {
                ContextElement
                    element = context.GetElementByDate(timeSeria
                        .Key); // GetDateElement(timeSeria.Key,context); // TimeSeriesData.FirstOrDefault<string, TimeSeries>(x => ( DateTime.Parse(x.key) == timeSeria.Key));

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }

                var values = TimeSeriesData[timeSeria.Key];

                System.Globalization.NumberFormatInfo numinf = new System.Globalization.NumberFormatInfo();
                numinf.NumberDecimalSeparator = ".";

                if (isIndex)
                {
                    element.OpenIndex = decimal.Parse(values.Open, numinf);
                    element.CloseIndex = decimal.Parse(values.Close, numinf);
                    element.HighIndex = decimal.Parse(values.High, numinf);
                    element.LowIndex = decimal.Parse(values.Low, numinf);
                    element.VolumeIndex = decimal.Parse(values.Volume, numinf);
                }
                else
                {
                    element.Open = decimal.Parse(values.Open, numinf);
                    element.Close = decimal.Parse(values.Close, numinf);
                    element.High = decimal.Parse(values.High, numinf);
                    element.Low = decimal.Parse(values.Low, numinf);
                    element.Volume = decimal.Parse(values.Volume, numinf);
                }

                string openHighLowClose =
                    string.Concat(values.Open, ",", values.Close, ",", values.Low, ",", values.High,
                        ","); // without volume
                element.Values += openHighLowClose;
            }
        }

        public void CleanseData()
        {
            {
                LogtoFile.LogMessagePrint("Call function CleanseData()");

                DateTime currentDate = DateTime.Now;
                DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

                int daysToRemove = (currentDate - startDateFromConfig).Days;

                var toRemove = TimeSeriesData.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove))
                    .ToList();

                for (int i = toRemove.Count - 1; i >= 0; i--)
                {
                    TimeSeriesData.Remove(toRemove[i].Key);
                }
            }
        }
    }
}