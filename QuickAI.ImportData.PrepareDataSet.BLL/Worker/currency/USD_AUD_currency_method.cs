﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.USD_AUD_currency;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker.currency
{
    public class USD_AUD_currency_method
    {
        public USD_AUD_currency_method()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataUSD_AUD MetadataUSD_AUD { get; set; }
        [JsonProperty(PropertyName = "Time Series FX (Daily)")]
        public Dictionary<string, TechnicalUSD_AUD> TechnicalUSD_AUD { get; set; }

        public static string GetUrl()
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "FX_DAILY&from_symbol=USD&outputsize=full&to_symbol=GBP");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalUSD_AUD)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalUSD_AUD[timeSeria.Key];

                element.Values = String.Concat(element.Values,",",values.CloseUSD_AUD);
            }
        }

        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalUSD_AUD.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalUSD_AUD.Remove(toRemove[i].Key);
            }
        }


    }
}
