﻿using System.Linq;

namespace QuickAI.ImportData.PrepareDataSet.BLL.NormalizeValues
{
    public class NormalizeVolumeIndex
    {
        
        public Context context;
        public NormalizeVolumeIndex(Context c)
        {
            context = c;
        }
        /* volume from index*/

        public void VolumeIndexCaluculate()
        {
            var volumeindexMax = context.Elements.Max(element => element.VolumeIndex); // find the max value of volume
            if (volumeindexMax != 0)
            {
                for (int i = 0; i < context.Elements.Count; i++)
                {
                    decimal volumeindex = context.Elements[i].VolumeIndex;
                    if (volumeindex != 0)
                    {
                        volumeindex = volumeindex / volumeindexMax;
                        var e = context.Elements[i];
                        string volumeindexstring = volumeindex.ToString();
                        var volindex = volumeindexstring.Replace(",", ".");
                        e.Values = string.Concat(e.Values, ",", volindex);
                    }
                }
            }
            

        }
    }
}
