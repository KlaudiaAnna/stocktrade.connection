﻿using System.Linq;

namespace QuickAI.ImportData.PrepareDataSet.BLL.NormalizeValues
{
   public class NormalizeADOSC
    {
        public Context context;
        public NormalizeADOSC(Context c)
        {
            context = c;
        } 
        /* ADOSC */
        public void ADOSCaluculate()
        {
            var ADOSCMax = context.Elements.Max(element => element.ADOSC);
            if (ADOSCMax != 0)
            {
                for (int i = 0; i < context.Elements.Count; i++)
                {
                    decimal ADOSC = context.Elements[i].ADOSC;
                    if (ADOSC != 0)
                    {
                        ADOSC = ADOSC / ADOSCMax;
                        var el = context.Elements[i];
                        string ADOSCstring = ADOSC.ToString();
                        var ADOSCindicator = ADOSCstring.Replace(",", ".");
                        el.Values = string.Concat(el.Values, ",", ADOSCindicator);
                    }
                }
            }
        }
    }
}
