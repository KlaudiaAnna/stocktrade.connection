﻿using System.Linq;
namespace QuickAI.ImportData.PrepareDataSet.BLL.NormalizeValues
{
    public class NormalizeAD
    {

        public Context context;
        public NormalizeAD(Context c)
        {
            context = c;
        }
        /* AD */
        public void ADCaluculate()
        {
            var adMaxp = context.Elements.Max(element => element.AD);
           
            for (int i = 0; i < context.Elements.Count; i++)
            {
                decimal ad = context.Elements[i].AD;
                if (ad != 0 && adMaxp != 0)
                {
                    ad = ad / adMaxp;
                    var el = context.Elements[i];
                    string adstring = ad.ToString();
                    var ADindicator = adstring.Replace(",", ".");
                    el.Values = string.Concat(el.Values, ",", ADindicator);
                }
            }
        }
    }
}

