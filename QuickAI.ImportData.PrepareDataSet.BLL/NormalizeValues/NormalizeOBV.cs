﻿using System.Linq;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;

namespace QuickAI.ImportData.PrepareDataSet.BLL.NormalizeValues
{
    public class NormalizeOBV
    {
        public Context context;
        public NormalizeOBV(Context c)
        {
            context = c;
        }
        public void OBVcalculate()
        {
            var OBVMax = context.Elements.Max(element => element.OBV);
            if (OBVMax != 0)
            {
                for (int i = 0; i < context.Elements.Count; i++)
                {
                    decimal obv = context.Elements[i].OBV;
                    if (obv != 0)
                    {
                        obv = obv / OBVMax;
                        var el = context.Elements[i];
                        string obvstring = obv.ToString();
                        var OBVindicator = obvstring.Replace(",", ".");
                        el.Values = string.Concat(el.Values, ",", OBVindicator);
                    }
                }
            }

        }
    }
}
