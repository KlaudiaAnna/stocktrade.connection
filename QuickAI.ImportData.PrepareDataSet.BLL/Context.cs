﻿using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using System.Collections.Generic;
namespace QuickAI.ImportData.PrepareDataSet.BLL
{
    public class Context
    {
        public Context()
        {
            Elements = new List<ContextElement>();
        }
        internal static IEnumerable<object> ContextElement;
        public List<ContextElement> Elements { get; set; }
        public ContextElement GetElementByDate(string date)
        {
            foreach (var item in Elements)
            {
                if (item.Data == date)
                    return item;
            }
            return null;
        }
    }
}

