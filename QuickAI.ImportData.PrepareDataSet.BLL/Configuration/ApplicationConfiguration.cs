﻿using System;
using System.Collections.Generic;
namespace QuickAI.ImportData.PrepareDataSet.BLL
{
    public class ApplicationConfiguration
    {
        public ApplicationConfiguration()
        { }
        // for Alpha
        public string ApiUrl { get; set; }
        public string ApiKey { get; set; }
        public string ApiKeyPremium { get; set;}
        public string RandomApiKey => RandomKey.GetKey();

        // for Twitter
        public string ApiUrlTwitter { get; set; }
        public string ApiKeytwitter { get; set; }
        public string ApiSecretKeyTwitter { get; set; }
        public string ApiUrlStocktwits { get; set; }
        public string TokenKeyTwitter { get; set; }
        public string TokenSecretKeyTwitter { get; set; }
        public string ConnectionString { get; set; }
        public int CounterTweet { get; set; }
        public int CounterWords { get; set; }

        /*Start Datetime from which */
        public DateTime StartDate { get; set; }
        public List<IndicatorInfo> Indicators { get; set; }
        public List<TickerInfo> Tickers { get; set; }
    }

    public class IndicatorInfo
    {
        public IndicatorInfo()
        { }
        public string Name { get; set; }
    }
    public class TickerInfo
    {
        public TickerInfo()
        { }

        public string Name { get; set; }
        public string Index { get; set; }
    }
    public class CurrencyInfo
    {
        public CurrencyInfo()
        { }
        public string From_symbol { get; set; }
        public string To_symbol { get; set; }
    }
}




