﻿using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using System;

namespace QuickAI.ImportData.PrepareDataSet.BLL
{
    public static class RandomKey
    {
        public static string GetKey()
        {
            string sKeys = Configuration.GetApplicationConfig().ApiKey;
            string[] keys = sKeys.Split(',');
            Random r = new Random(DateTime.Now.Millisecond);
            int i = r.Next(0, keys.Length);
            return keys[i];
        }
    }
}
