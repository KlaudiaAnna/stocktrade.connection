﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
namespace QuickAI.ImportData.PrepareDataSet.BLL.SavingProcess
{
    /* Save all content of downloaded tweets from Twitter before setting to Database */
    public class SaveTweets
    {
        public SaveTweets() 
        {

        }
       string NewPath = ConfigurationManager.AppSettings["PathToTweet"];
        public void SavetoTxt<T>(List<T> tw, string portalname, string ticker)
        {
            string date = DateTime.Now.ToString("yyyy-dd-MM");
            var s = NewPath + date + "_" + portalname + "_" + ticker + ".txt";
            if (File.Exists(s))
            {
                File.Delete(s);
            }
            using (StreamWriter writetext = new StreamWriter(s))
            {
                foreach (T entry in tw)
                {
                    foreach (var property in typeof(T).GetProperties())
                    {
                        var propertyValue = property.GetValue(entry);
                        writetext.WriteLine(propertyValue);
                    }
                }
            }
        }
    }
}


