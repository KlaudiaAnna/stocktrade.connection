﻿using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
namespace QuickAI.ImportData.PrepareDataSet.BLL.SavingProcess
{
    public class SaveCSV
    {
        private const int count_lines = 58;

        public bool savecountercheck = false;
        readonly string NewPath = ConfigurationManager.AppSettings["Path"];
        public Context context;
        public SaveCSV(Context c)
        {
            context = c;
        }
        public bool Save_Original(string fileName)
        {
            LogtoFile.LogMessagePrint(string.Format("Save to file {0}", fileName));
            fileName = string.Concat(fileName + "_original", ".csv");

            var name_directory = NewPath + DateTime.Now.ToString("yyMMdd");
         
            if (!File.Exists(name_directory))
            {
                Directory.CreateDirectory(name_directory);
            }
            string name = Path.Combine(name_directory, fileName);

            if (File.Exists(name))
            {
                File.Delete(name);
            }
            foreach (var e in context.Elements)
            {
                var lines = e.Values.Split(',').Count();
                if (lines < count_lines) continue;
                File.AppendAllText(name, e.Values);
                File.AppendAllText(name, Environment.NewLine);
                savecountercheck = true;
            }
            return savecountercheck;
        }

        public bool Save_Classifier(string fileName)
        {
            LogtoFile.LogMessagePrint(string.Format("Save to file classifer {0}", fileName));
            fileName = string.Concat(fileName, "_classifier", ".csv");
            var name_directory = NewPath + DateTime.Now.ToString("yyMMdd");

            if (!File.Exists(name_directory))
            {
                Directory.CreateDirectory(name_directory);
            }
            string name = Path.Combine(name_directory, fileName);

            if (File.Exists(name))
            {
                File.Delete(name);
            }

            for (int i = 0; i < context.Elements.Count - 1; i++)
            {
                var checklines = context.Elements[i].Values.Split(',').Count() < count_lines;
                var values = context.Elements[i].Values;
                if (checklines)
                    continue;
                if (i == context.Elements.Count - 1)
                {
                    File.AppendAllText(name, values + "," + "0");
                }

                decimal currClose = context.Elements[i].Close;

                decimal prevClose = context.Elements[i + 1].Close;

                if (prevClose > currClose)
                {
                    File.AppendAllText(name, values + "," + "1" + Environment.NewLine);
                }
                else
                {
                    File.AppendAllText(name, values + "," + "0" + Environment.NewLine);
                }

                savecountercheck = true;


            }
            return savecountercheck;
        }
        public bool Save_Reversed(string fileName)
        {
            var sorted = context.Elements
                .SelectMany(a => a.Data)
                .OrderBy(a => a)
                .Distinct()
                .ToList();
            LogtoFile.LogMessagePrint(string.Format("Save to file reversed {0}", fileName));
            context.Elements = context.Elements.OrderBy(a => a.Data).ToList();
            fileName = string.Concat(fileName, "_reserved", ".csv");
            var name_directory = NewPath + DateTime.Now.ToString("yyMMdd");

            if (!File.Exists(name_directory))
            {
                Directory.CreateDirectory(name_directory);
            }
            string name = Path.Combine(name_directory, fileName);

            if (File.Exists(name))
            {
                File.Delete(name);
            }
            foreach (var e in context.Elements)
            {
                var lines = e.Values.Split(',').Count();
                if (lines < count_lines) continue;
                File.AppendAllText(name, e.Values);
                File.AppendAllText(name, Environment.NewLine);
                savecountercheck = true;
            }
            return savecountercheck;
        }
    }
}
