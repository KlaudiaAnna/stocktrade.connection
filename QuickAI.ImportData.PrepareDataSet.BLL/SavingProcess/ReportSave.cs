﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
namespace QuickAI.ImportData.PrepareDataSet.BLL.SavingProcess
{
    public class ReportSave
    {
        private readonly string ticker = string.Empty;
        readonly string pathreport = ConfigurationManager.AppSettings["PathReport"];
        public ReportSave(string ticker)
        {
            this.ticker = ticker;
        }

        /* Report State for Alpha tickers*/

        public void Reporting(bool saveorig, bool saverever, bool saveclas)
        {
            using (StreamWriter sw = new StreamWriter(pathreport + "Report_Alpha_GenerateDataSets_" + DateTime.Now.ToString("yyyy-dd-MM") + ".txt", true))
            {
                if (saveorig)
                    sw.WriteLine(DateTime.Now.ToString() + "  Generated file for {0} for function original", ticker);
                if (!saveorig)
                    sw.WriteLine(DateTime.Now.ToString() + "  NOT generated file for {0} for function _original", ticker);
                if (saverever)
                    sw.WriteLine(DateTime.Now.ToString() + "  Generated file for {0} for function reversed", ticker);
                if (!saverever)
                    sw.WriteLine(DateTime.Now.ToString() + "  NOT generated file for {0} for function reversed", ticker);
                if (saveclas)
                    sw.WriteLine(DateTime.Now.ToString() + "  Generated file for {0} for function clasifier", ticker);
                if (!saveclas)
                    sw.WriteLine(DateTime.Now.ToString() + "  NOT generated file for {0} for function clasifier", ticker);
            }
        }

        public void Reporting_notvalid()
        {
            using (StreamWriter sw =
                new StreamWriter(
                    pathreport + "Report_Alpha_GenerateDataSets_FAILED" + DateTime.Now.ToString("yyyy-dd-MM") + ".txt", true))
            {
                sw.WriteLine(DateTime.Now.ToString() + "ProccessData cannot download data for tickers {0}",
                    ticker);

                sw.Close();
            }
        }

    }
}
