﻿using MongoDB.Driver;
using QuickAI.ImportData.PrepareDataSet.DAL.Collection;
using QuickAI.ImportData.PrepareDataSet.DAL.Properties;
namespace QuickAI.ImportData.PrepareDataSet.DAL
{
   public class ControllerMongoSb
    {
        public IMongoDatabase databaseBase;
        public IMongoCollection<Tweet> collection;
        public ControllerMongoSb()
        {
            var client = new MongoClient(Settings.Default.QuickConnection);
            databaseBase = client.GetDatabase(Settings.Default.Tickersdata);
            collection = databaseBase.GetCollection<Tweet>("tweets");
        }
    }
}
