﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MongoDB.Driver;
using QuickAI.ImportData.PrepareDataSet.DAL.Collection;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
namespace QuickAI.ImportData.PrepareDataSet.DAL
{
    public class Insert : ControllerMongoSb
    {
        private Tweet _tw = new Tweet();
        public IEnumerable<TweetCollectionCompleted> InsertToCollection(IEnumerable<TweetCollectionCompleted> list)
        {
            try
            {
                foreach (var item in list)
                {
                    bool exists = collection.AsQueryable().Any(ele => ele.Id_Tweet == item.IdTweet);
                    bool existstweetsame = collection.AsQueryable().Any(el => el.Text == item.Text);
                    if (!exists || !existstweetsame)
                    {
                        _tw = new Tweet
                        {
                            Symbol = item.Symbol,
                            Id_Tweet = item.IdTweet,
                            Date = item.Date,
                            ID_User = item.IdUser,
                            User_Name = item.UserName,
                            Text = item.Text,
                            Portal = item.Portal,
                            Date_Inserted = DateTime.Now.ToString(CultureInfo.CurrentCulture),
                            Human_rating = String.Empty,
                            Neural_rating = String.Empty
                        };
                        collection.InsertOne(_tw);
                    }
                }
                return list;
            }
            catch (Exception e)
            {
                var exEMessage = e.Message;
                return null;
            }
        }
    }
}

