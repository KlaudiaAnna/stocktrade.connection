﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using System.Configuration;
using QuickAI.ImportData.PrepareDataSet.DAL.Model;
namespace QuickAI.ImportData.PrepareDataSet.DAL
{
    public class SerializeToJson
    {
        JsonSerializer serializer = new JsonSerializer();
        List<DataDB> elem = new List<DataDB>();
        List<DataDBelement> l = new List<DataDBelement>();
        readonly string path = ConfigurationManager.AppSettings["Json_Path"];
        public void SetoJson(IEnumerable<TweetCollectionCompleted> list)
        {
            foreach (var i in list)
            {
                string jpath = path + "DataBaseQuick_" + DateTime.Now.ToString("yyyy-dd-MM") + ".Json";

                if (File.Exists(jpath))
                {
                    var jsondata = File.ReadAllText(jpath);
                    var read = JsonConvert.DeserializeObject<List<DataDB>>(jsondata);
                    read.Add(new DataDB() { elem = l });
                    l.Add(new DataDBelement() { Data = DateTime.Now.ToShortDateString(), Portal = i.Portal, TickerName = i.Symbol });
                    using (StreamWriter file = new StreamWriter(jpath))
                    using (JsonWriter writer = new JsonTextWriter(file))
                    {
                        serializer.Serialize(writer, l);
                    }
                }
                else
                {
                    elem.Add(new DataDB() { elem = l });
                    l.Add(new DataDBelement() { Data = DateTime.Now.ToShortDateString(), Portal = i.Portal, TickerName = i.Symbol });
                    using (StreamWriter file = new StreamWriter(jpath))
                    using (JsonWriter writer = new JsonTextWriter(file))
                    {
                        serializer.Serialize(writer, l);
                    }
                }
            }
        }
    }
}

