﻿using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
namespace QuickAI.ImportData.PrepareDataSet.DAL.Report
{
    public static class Report
    {
        public static string ticker;
        readonly static string pathreport = ConfigurationManager.AppSettings["PathReport"];
        /* Report State for inserted tickers from stocktwits and twitter*/

        public static void Reporting(IEnumerable<TweetCollectionCompleted> list)
        {
            using (StreamWriter sw = new StreamWriter(pathreport + "Report_MongoInserted_SetsToDatabse_" + DateTime.Now.ToString("yyyy-dd-MM") + ".txt", true))
            {
                foreach (var el in list)
                {
                    if (ticker != el.Symbol)
                    {
                        sw.WriteLine("Inserted: " + el.Symbol + "Tweet:" + el.Text + " Data:" + el.DateInsertedDb);
                        el.Symbol = ticker;
                    }
                }
                sw.Close();
            }
        }
    }
}


