﻿using System;
using MongoDB.Driver;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
namespace QuickAI.ImportData.PrepareDataSet.DAL
{
    public class ReadDataDatabase : ControllerMongoSb, IDataFromMongo
    {
        private const string V = "Stocktwits";
        public long ReadData()
        {
            var result = collection.Find(e => e.Date < DateTime.Today && e.Portal == V).SortByDescending(a => a.Date).ToList();

            if (result.Count == 0)
            {
                return 0;
            }
            foreach (var elem in result)
            {
                return elem.Id_Tweet;
            }
            return ReadData();
        }
    }
}  
