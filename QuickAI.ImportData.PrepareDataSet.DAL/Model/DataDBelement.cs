﻿using Newtonsoft.Json;
namespace QuickAI.ImportData.PrepareDataSet.DAL.Model
{
    public class DataDBelement
    {
        [JsonProperty(PropertyName = "Ticker")]
        public string TickerName { get; set; }
        [JsonProperty(PropertyName = "Portal")]
        public string Portal { get; set; }
        [JsonProperty(PropertyName = "Data")]
        public string Data { get; set; }
    }
}
