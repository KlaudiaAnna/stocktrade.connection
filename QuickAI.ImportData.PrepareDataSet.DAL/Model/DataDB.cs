﻿using System.Collections.Generic;
using System.Runtime.Serialization;
namespace QuickAI.ImportData.PrepareDataSet.DAL.Model
{
    [DataContract]
    public class DataDB
    {
        [DataMember(Name = "DataDBelements")]
        public List<DataDBelement> elem = new List<DataDBelement>();
    }
}
