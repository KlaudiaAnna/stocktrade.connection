﻿using System;
using System.Collections.Generic;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickAI.ImportData.PrepareDataSet.DAL.Report;
namespace QuickAI.ImportData.PrepareDataSet.DAL
{
    public class DataToDb : IDataMongo
    {
        public List<TweetCollectionCompleted> list;
        public DataToDb(List<TweetCollectionCompleted> list)
        {
            this.list = list;
        }
        public bool SetData()
        {
            try
            {
                Insert insert = new Insert();
                var lista = insert.InsertToCollection(list);
                SerializeToJson json = new SerializeToJson();
                json.SetoJson(lista);
                Report.Report.Reporting(lista);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
