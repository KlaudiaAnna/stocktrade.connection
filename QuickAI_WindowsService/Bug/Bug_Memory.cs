﻿using System;
using System.Collections.Generic;
namespace QuickAI_WindowsService.Bug
{
    public class Bug_Memory : QuickAI
    {
        public Dictionary<DateTime, string> Errors { get; set; }

        public Bug_Memory(Exception ex) : base(ex)
        {
            Errors = new Dictionary<DateTime, string>();
        }
        public Bug_Memory() : this(null) { }
    }
}

