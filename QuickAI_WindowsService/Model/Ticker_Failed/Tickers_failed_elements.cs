﻿using System.Collections.Generic;
using System.Runtime.Serialization;
namespace QuickAI_WindowsService.Ticker_Failed
{
    [DataContract]
    public class Tickers_failed_elements
    {
        [DataMember]
        public List<Ticker_failed_l> Failed_tickers { get; set; }
    }
}

