﻿using System.Runtime.Serialization;
namespace QuickAI_WindowsService.Ticker_Failed
{ 
    [DataContract]
    public class Ticker_failed_l
    {
        [DataMember]
        public string Tickername { get; set; }
        [DataMember]
        public string Index { get; set; }
    }
}