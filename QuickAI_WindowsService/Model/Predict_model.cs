﻿using System;
using System.Runtime.Serialization;
namespace QuickAI_WindowsService.Model
{
    [DataContract]
    public class Predict_model
    {
        [DataMember]
        public DateTime date { get; set; }
        public bool Execute { get; set; }
    }
}
