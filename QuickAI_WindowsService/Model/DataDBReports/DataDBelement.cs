﻿using System.Runtime.Serialization;
namespace QuickAI.ImportData.PrepareDataSet.Model.DataDBReports
{
    [DataContract]
    public class DataDBelement
    {
        [DataMember(Name = "Ticker")]
        public string TickerName { get; set; }
        [DataMember(Name = "Portal")]
        public string Portal { get; set; }
        [DataMember(Name = "Data")]
        public string Data { get; set; }
    }
}
