﻿using System;
using System.Runtime.Serialization;
namespace QuickAI_WindowsService.Model
{
    [DataContract]
    public class LearningElelments
    {
        [DataMember(Name = "Id")]
        public string Id { get; set; }
        [DataMember(Name = "Date")]
        public DateTime Date { get; set; }
        [DataMember(Name = "Ticker")]
        public string Ticker { get; set; }
        [DataMember(Name = "Teach")]
        public bool Teach { get; set; }
        [DataMember(Name = "Price_1")]
        public decimal Price_1 { get; set; }
        [DataMember(Name = "Price_2")]
        public decimal Price_2 { get; set; }
        [DataMember(Name = "Price_3")]
        public decimal Price_3 { get; set; }
    }
}
