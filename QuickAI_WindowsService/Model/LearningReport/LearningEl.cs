﻿using System.Collections.Generic;
using System.Runtime.Serialization;
namespace QuickAI_WindowsService.Model.LearningReport
{
   public class LearningEl
    {
        [DataMember(Name = "Elements of predict")]
        List<LearningElelments> status_ele { get; set; }
    }
}
