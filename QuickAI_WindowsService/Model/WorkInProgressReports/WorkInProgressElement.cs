﻿using System.Runtime.Serialization;
namespace QuickAI.ImportData.PrepareDataSet.Model.WorkInProgressReports
{
    [DataContract]
    public class WorkInProgressElement
    {
        [DataMember(Name = "Function original")]
        public bool Original { get; set; }
        [DataMember(Name = "Function reversed")]
        public bool Reversed { get; set; }
        [DataMember(Name = "Function clasifier")]
        public bool Clasifier { get; set; }
        [DataMember(Name = "Ticker")]
        public string TickerName { get; set; }
        [DataMember(Name = "Date")]
        public string Date { get; set; }
    }
}
