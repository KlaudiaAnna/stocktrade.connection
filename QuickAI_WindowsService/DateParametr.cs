﻿using System;
using System.Configuration;
namespace QuickAI_WindowsService
{
    public abstract class DateParametr
    {
        public static ValueTuple<int,int,int,int, DayOfWeek, DayOfWeek,DayOfWeek>  GetParamtrTime() 
        {
            string counter = ConfigurationManager.AppSettings["Counter"];
            int count = int.Parse(counter);
            string startTime = ConfigurationManager.AppSettings["StartTime"];
            DateTime time = DateTime.Parse(startTime);

            int hConfig = time.Hour;
            int mConfig = time.Minute;
            string startTeach = ConfigurationManager.AppSettings["StartTeach"];
            DateTime timetTime = DateTime.Parse(startTeach);
            int hour = timetTime.Hour;
            int minut = timetTime.Minute;

            string Monday = ConfigurationManager.AppSettings["Monday"];
            string Friday = ConfigurationManager.AppSettings["Friday"];
            string Saturday = ConfigurationManager.AppSettings["Saturday"];

            DayOfWeek Saturday_conf = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), Saturday);
            DayOfWeek Monday_conf = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), Monday);
            DayOfWeek Friday_conf = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), Friday);

            return new ValueTuple<int, int, int, int, DayOfWeek, DayOfWeek, DayOfWeek> (mConfig, hConfig, count, hour, Saturday_conf, Monday_conf, Friday_conf);
        }
    }
}
