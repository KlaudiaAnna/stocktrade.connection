﻿using QuickAI_WindowsService.LearningManager;
using QuickAI_WindowsService.Model;
using System;
using System.Collections.Generic;
namespace QuickAI_WindowsService.Learning
{
    public class Teach
    {
        public List<LearningElelments> tickersLists = new List<LearningElelments>();
        public Teach(List<LearningElelments> lists)
        {
            lists = tickersLists;
        }

        public  List<LearningElelments> SpecifyState(string ticker)
        {
            try
            {
                QuickAIManagerClient clienttAiManagerClient = new QuickAIManagerClient();

                Log.LogMessage(string.Format("try teach for ticker {0}", ticker));
                bool learn = clienttAiManagerClient.TeachTheModel(ticker);
                if (learn == true)
                {
                    tickersLists.Add(new LearningElelments
                    {
                        Teach = learn,
                        Ticker = ticker
                    });
                }
                return tickersLists;
            }
            catch (Exception ex)
            {
                Log.LogMessage(string.Format("Exception occured while running GetDataSet: {0}, {1}", ex.Message, ex.InnerException));
                return null;
            }
        }
    }
}
