﻿using QuickAI_WindowsService.LearningManager;
using QuickAI_WindowsService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
namespace QuickAI_WindowsService
{
    public static class Predict
    {
        public static List<LearningElelments>  PredictState(List<LearningElelments> list)
        {
            try
            {
                QuickAIManagerClient quickAI = new QuickAIManagerClient();
                List<LearningElelments> predictlist = new List<LearningElelments>();
                foreach (var el in list)
                {
                    if (el.Teach == true)
                    {
                        Log.LogMessage(string.Format("execute predict for ticker {0}", el.Ticker));
                        decimal[] predict = quickAI.Predict(el.Ticker);
                        predictlist.Add(new LearningElelments
                        {
                            Ticker = el.Ticker,
                            Price_1 = predict.ElementAt(0),
                            Price_2 = predict.ElementAt(1),
                            Price_3 = predict.ElementAt(2)
                        });
                    }
                }
                return predictlist;
            }
            catch (Exception ex)
            {
                Log.LogMessage(string.Format("Exception occured while running : {0}, {1}", ex.Message, ex.InnerException));
                return null;
            }
        }
    }
}
