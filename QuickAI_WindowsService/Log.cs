﻿using System;
using System.Configuration;
using System.IO;
namespace QuickAI_WindowsService
{
    public static class Log
    {
        public static void LogMessage(string message)
        {
            try
            {
                string path2 = ConfigurationManager.AppSettings["log_path"];
                string namefile = "log_service_" + DateTime.Now.ToString("yyyy-dd-MM") + ".txt";
                using (StreamWriter sw = new StreamWriter(path2 + namefile, true))
                {
                    sw.WriteLine(DateTime.Now + "  " + message);
                    sw.Close();
                }
            }
            catch { LogMessageError(message); }
        }

        public static void LogMessageError(string message)
        {
            try
            {
                string path2 = ConfigurationManager.AppSettings["log_path"];

                using (StreamWriter sw = new StreamWriter(path2 + "log_catch" + DateTime.Now.ToString("yyyy-dd-MM") + ".txt", true))
                {
                    sw.WriteLine(DateTime.Now + "  " + message);
                    sw.Close();
                }
            }
            catch { }
        }
    }
}
