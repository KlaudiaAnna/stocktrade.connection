﻿using QuickAI_WindowsService.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
namespace QuickAI_WindowsService
{
    public static class Save
    {
        public static void SaveProcess(List<LearningElelments> list)
        {
            var path = ConfigurationManager.AppSettings["State_Tickers"];
             string name_directory = path + DateTime.Now.ToString("yyMMdd");
            string file = Path.Combine(name_directory, "State.csv");

            if (!Directory.Exists(name_directory))
            {
                Directory.CreateDirectory(name_directory);
            }

            if (File.Exists(file))
            {
                File.Delete(file);
            }

            using (StreamWriter writetext = new StreamWriter(file, true))
            {
                foreach (var l in list)
                {
                    writetext.WriteLine(DateTime.Now.ToShortDateString() + "," + l.Ticker + "," + l.Price_1 + "," + l.Price_2 + "," + l.Price_3);
                }
            }
        }
    }
}



