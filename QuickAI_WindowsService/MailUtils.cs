﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
namespace QuickAI_WindowsService
{
    public static class MailUtils
    {
        public static void SendEmail(string subject, string body)
        {
            try
            {
                /********************************GET CONFIGURATION PASSWORD*****************************************************************/
                var cipher = ConfigurationManager.AppSettings["cipherText"];
                var passw1 = StringCripting.DecryptString(cipher, "neurowa0987awsd");

                var toAdress = new MailAddress(ConfigurationManager.AppSettings["toAddress"]);
                var fromAddress = new MailAddress(ConfigurationManager.AppSettings["fromAddress"]);

                /********************************GET CONFIGURATION FROM EMAIL********************************************************************/
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = true,
                    Credentials = new NetworkCredential("quick.ai.test@gmail.com", passw1)
                };

                /*******************************BODY SUBJECT && SEND******************************************************************************/

                using (var message = new MailMessage(fromAddress, toAdress)
                {
                    Subject = subject,
                    Body = body
                })
                    smtp.Send(message);
            }
            catch (Exception ex)
            {
                Log.LogMessage(string.Format("Cannot send email {0}{1}", ex.InnerException, ex.Message ));
            }
        }
    }
}