﻿using System;
using System.Globalization;
using System.ServiceProcess;
using System.Threading;
using QuickAI_WindowsService.Learning;
using QuickAI_WindowsService.SerializeDeserializeJson;
using QuickAI_WindowsService.ObtainDataService;
using System.Collections.Generic;
using QuickAI_WindowsService.Model;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using QuickAI_WindowsService.Ticker_Failed;
using QuickAI_WindowsService.Bug;
using System.Linq;

namespace QuickAI_WindowsService
{
    public partial class QuickAI : ServiceBase
    {
        Thread t;
        private ManualResetEvent shutdown;
        private Exception ex;

        public QuickAI()
        {
            shutdown = new ManualResetEvent(false);
            t = null;
            InitializeComponent();
        }


        public QuickAI(Exception ex)
        {
            this.ex = ex;
        }


        internal void Start(string[] args)
        {

        }
        protected override void OnStart(string[] args)
        {
            try
            {
                Log.LogMessage("In start method " + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                base.OnStart(args);
                Log.LogMessage("try execute DoWork" + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                t = new Thread(DoWork);
                t.Start();
            }
            catch (Exception ex)
            {
                Log.LogMessage(string.Format("Exception occured In start method", ex.Message, ex.InnerException));
            }
        }
        private void DoWork()
        {
            try
            {

                Log.LogMessage("Waiting");
                List<Tickers_failed_elements> load_failed = new List<Tickers_failed_elements>();
                string guid = Guid.NewGuid().ToString();
                Serialize jSave = new Serialize();
                var config = DateParametr.GetParamtrTime();
                while (!shutdown.WaitOne(0))
                {
                    if ((DateTime.Now.Hour >= config.Item2) && ((DateTime.Now.Hour <= config.Item2 + 2) && (DateTime.Now.DayOfWeek >= config.Item6) && (DateTime.Now.DayOfWeek <= config.Item7)))
                    {
                        string path = ConfigurationManager.AppSettings["Json_Path"];
                        string jpath = path + "TickerFailed_" + DateTime.Now.ToString("yyyy-dd-MM") + ".json";
                        string pathtofailedjson = Path.Combine(path, jpath);
                        if (File.Exists(pathtofailedjson))
                        {
                            var jsondata = File.ReadAllText(pathtofailedjson);
                            load_failed = JsonConvert.DeserializeObject<List<Tickers_failed_elements>>(jsondata);
                        }
                        Log.LogMessage("Checked time, aplication started");
                        var load_alpha = Deserialize.LoadJson_Alpha("Alpha_" + DateTime.Now.ToString("yyyy-dd-MM") + ".json");
                        var load_datebase = Deserialize.LoadJson_DB("DataBaseQuick_" + DateTime.Now.ToString("yyyy-dd-MM") + ".json");
                        var load_teaching_status = Deserialize.LoadJson_Teachstatus("Teaching_status_" + DateTime.Now.ToString("yyyy-dd-MM") + ".Json");
                        var load_predict_status = Deserialize.load_predict_status("Predict_status_" + DateTime.Now.ToString("yyyy-dd-MM") + ".Json");
                        bool resultdatabase, result;
                        resultdatabase = result = false;
                        bool alph, dat;
                        alph = dat = false;
                        List<LearningElelments> tickersLists = new List<LearningElelments>();
                        /* If json file is emty or diffrent date not today */
                        if (load_alpha == null)
                        {
                            Log.LogMessage("Call method GetDataSet()");
                            ObtainDataClient obtainDataClient = new ObtainDataClient();
                            result = obtainDataClient.ReturnedTrueorFalse();
                            Log.LogMessage(string.Format("method GetDataSet() returned {0} ", result));
                            alph = true;
                        }

                        if (load_datebase == null)
                        {
                            DataMongoClient dataMongoClient = new DataMongoClient();
                            Log.LogMessage("Call method database()");
                            resultdatabase = dataMongoClient.SetsDatabse();
                            Log.LogMessage(string.Format("method GetDataSet() returned {0} ", resultdatabase));
                            dat = true;
                        }
                        /***************************************Sending Email about stateof Obtain and Database**********************************************************************************************/
                        if (dat == true && alph == true)
                        {
                            Log.LogMessage("Send email ");
                            MailUtils.SendEmail("QuickAI Windows Services - returned: ",
                              String.Format("Function returned: from Generate {0} from database:{1}", result, resultdatabase));
                        }
                        /*****************************************Get tickers list*****************************************************************************************************/
                        if ((DateTime.Now.Hour >= config.Item2) && (DateTime.Now.Hour <= config.Item2 + 2) && (DateTime.Now.DayOfWeek >= config.Item6) && (DateTime.Now.DayOfWeek <= config.Item7))
                        {
                            if ((load_alpha != null && load_teaching_status == null))
                            {
                                ObtainDataClient dataClient = new ObtainDataClient();
                                Log.LogMessage("Get ticker lists");
                                var listTickers = dataClient.TickerswithOpenCloseBol();
                                Log.LogMessage("generated csv files for alpha");
                                foreach (var ticker in listTickers)
                                {
                                    Log.LogMessage("Started Teaching");
                                    Teach teach = new Teach(tickersLists);
                                    if (tickersLists.Any(x => x.Ticker != ticker.TickerName))
                                    {
                                        tickersLists = teach.SpecifyState(ticker.TickerName);
                                        jSave.SetToJsonTikersList(tickersLists);
                                    }
                                }
                            }
                        }
                        /*Date in weekend */
                        var teach_elem = Deserialize.LoadJson_Teachstatus("Teaching_status_" + DateTime.Now.AddDays(-1).ToString("yyyy-dd-MM") + ".Json");
                        if ((teach_elem != null) && /*(DateTime.Now.DayOfWeek == config.Item5) && */(load_predict_status == null))
                        {
                            Log.LogMessage("Load teaching go to prediction");
                            var after_predict = Predict.PredictState(teach_elem);
                            Save.SaveProcess(after_predict); jSave.Ser_Predict();
                        }
                    }
                    Log.LogMessage("Go to sleep");
                    Thread.Sleep(61000);
                }
            }
            catch (Exception ex)
            {
                Adddict el = new Adddict();
                el.add(ex);
                Log.LogMessage(string.Format("Exception occured while running : {0}, {1}", ex.Message, ex.InnerException));
                MailUtils.SendEmail("QuickAI Windows Services - Fatal Exception",
                 string.Format("Exception occured while running : {0}, {1}", ex.Message, ex.InnerException));
            }
        }
        protected override void OnStop()
        {
            base.OnStop();
            try
            {
                Log.LogMessage("Stop Services " + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                t.Abort();
                Log.LogMessage("Has stopped? " + t.IsAlive);
            }
            catch (Exception ex)
            {
                Log.LogMessage(string.Format("Exception occured while running : {0}, {1}", ex.Message, ex.InnerException));
                Log.LogMessage(ex.Message);
                if (ex.InnerException != null) Log.LogMessage(ex.InnerException.ToString());
            }
        }
    }
}

