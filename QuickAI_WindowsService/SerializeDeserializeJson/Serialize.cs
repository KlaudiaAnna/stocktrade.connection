﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using QuickAI_WindowsService.Model;
using System.IO;
using System.Configuration;
namespace QuickAI_WindowsService.SerializeDeserializeJson
{
    public class Serialize
    {
        List<LearningElelments> wroteTickers = new List<LearningElelments>();
        Predict_model pred = new Predict_model();
        readonly string save = ConfigurationManager.AppSettings["Json_Path"];
        JsonSerializer serializer = new JsonSerializer();
    
        public void SetToJsonTikersList(List<LearningElelments> tickers)
        {
            foreach (var el in tickers)
            {
                using (StreamWriter file = new StreamWriter(save + "Teaching_status_" + DateTime.Now.ToString("yyyy-dd-MM") + ".Json"))
                {
                    var id = IdGenarator.GenerateID();
                    wroteTickers.Add(new LearningElelments()
                    {
                        Date = DateTime.Now,
                        Id = id,
                        Ticker = el.Ticker,
                        Teach = el.Teach
                    });
                    serializer.Serialize(file, wroteTickers);
                }
            }
        }
        public void Ser_Predict()
        {

            using (StreamWriter file = new StreamWriter(save + "Predict_status_" + DateTime.Now.ToString("yyyy-dd-MM") + ".Json"))
            {
                pred.date = DateTime.Now;
                pred.Execute = true;
                serializer.Serialize(file, pred);

            }
        }
    }
}

