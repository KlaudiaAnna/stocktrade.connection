﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using QuickAI_WindowsService.Model;
using QuickAI.ImportData.PrepareDataSet.Model.WorkInProgressReports;
using QuickAI.ImportData.PrepareDataSet.Model.DataDBReports;
using System;

namespace QuickAI_WindowsService.SerializeDeserializeJson
{
    public static class Deserialize
    {
        public static string jsonfile = ConfigurationManager.AppSettings["Json_Path"];
   

        public static List<WorkInProgress> LoadJson_Alpha(string filename)
        {
            string checkifexist = jsonfile + filename;
            if (File.Exists(checkifexist))
            {
                using (StreamReader read = new StreamReader(checkifexist))
                {
                    string json = read.ReadToEnd();
                    var  workerlist = JsonConvert.DeserializeObject<List<WorkInProgress>>(json);
                    return workerlist;
                }
            }
            return null;
        }

        public static List<DataDB> LoadJson_DB(string filename)
        {
            string checkifexist = jsonfile + filename;

            if (File.Exists(checkifexist))
            {
                using (StreamReader read = new StreamReader(checkifexist))
                {
                    string json = read.ReadToEnd();
                    var GetList = JsonConvert.DeserializeObject<List<DataDB>>(json);
                    return GetList;
                }
            }
            return null;
        }

        public static List<LearningElelments> LoadJson_Teachstatus(string filename)
        {
            string checkifexist = jsonfile + filename;
            if (File.Exists(checkifexist))
            {
                using (StreamReader read = new StreamReader(checkifexist))
                {
                    string json = read.ReadToEnd();
                    var GetList = JsonConvert.DeserializeObject<List<LearningElelments>>(json);

                    return GetList;
                }
            }
            return null;
        }

        public static object load_predict_status(string filename)
        {
            string checkifexist = jsonfile + filename;
            if (File.Exists(checkifexist))
            {
                using (StreamReader read = new StreamReader(checkifexist))
                {
                    string json = read.ReadToEnd();
                    Predict_model obj = JsonConvert.DeserializeObject<Predict_model>(json);
                    return obj;
                }
            }
            return null;
        }
    }
}
